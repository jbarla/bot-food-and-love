<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_order_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("item_sku");
            $table->string("attributeSelected")->nullable();
            $table->string("qty");
            $table->string("name");
            $table->string("price");
            $table->bigInteger('order_id')->unsigned()->index(); // this is working
            $table->foreign('order_id')->references('id')->on('ecommerce_order')->onDelete('cascade');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_order_items');
    }
}
