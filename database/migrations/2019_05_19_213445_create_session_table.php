<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session', function (Blueprint $table) {
            //$table->increments('id');

            $table->string('session_id', 191)->unique();


            /*$table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users');*/
            
            //Usuario de la platafomra
            //$table->unsignedInteger('user_id');
            //$table->foreign('user_id')->references('id')->on('users');

            //Cliente que se comunica con el usuario
            $table->string('company_id', 191);

            $table->text('metadata')->nullable();

            $table->text('channel')->nullable();

            $table->string('mode')->default("bot");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('session', function (Blueprint $table) {
            //
        });
    }
}
