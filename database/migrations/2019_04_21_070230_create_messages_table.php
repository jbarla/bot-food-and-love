<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');

            //user
            $table->string('from_id');
            $table->string('direction');

            $table->string('status')->default("unread");
            /*$table->foreign('from_id')->references('id')->on('users');*/

            //contact

            //contenido del mensaje
            //$table->string('type_content')->default("text");
            $table->string('type')->default("text");
            $table->mediumText('content');

            $table->text('metadata')->nullable();

            $table->text('channel')->nullable();


            $table->dateTime('message_time')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
