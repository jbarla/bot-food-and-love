<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_cart_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("item_sku");
            $table->string("attributeSelected")->nullable();
            $table->string("qty");
            $table->string("name");
            $table->string("price");
            $table->bigInteger('cart_id')->unsigned()->index(); // this is working
            $table->foreign('cart_id')->references('id')->on('ecommerce_cart')->onDelete('cascade');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_cart_items');
    }
}
