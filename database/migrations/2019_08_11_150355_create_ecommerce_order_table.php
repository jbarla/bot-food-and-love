<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcommerceOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_order', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("session_id");
            $table->string("total");
            $table->string("shopping_address");
            $table->string("payment_method");
            $table->string("contact_phone");
            $table->string("status")->default("new");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_order');
    }
}
