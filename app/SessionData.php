<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionData extends Model
{
    protected $table = "session_data";
}
