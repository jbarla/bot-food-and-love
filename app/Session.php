<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table = "session";
    protected $primaryKey = "session_id";
    public $incrementing = false;

    protected $fillable = [
        'updated_at'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function contact(){

        return $this->belongsTo(User::class);
    }

    public function storeMessageIn($params)
    {
        $content = json_decode($params["message"]);
        $sessionId = $params["sessionId"];
        $message = new Message();

        $message->from_id = $sessionId;
        $message->direction = "in";
        if($content->type == "text"){
            $message->content = $content->content;
        }elseif($content->type == "location"){
            $message->content = $params["message"];
        }else{
            $message->content = $params["message"];
        }

        $message->metadata = null;
        $message->type = $content->type;
        $message->channel = "webchat";
        $message->message_time = date("Y-m-d h:i:s");
        $message->save();
    }

    public function storeMessageOut($result, $sessionId)
    {
        $message = new Message();

        $message->from_id = $sessionId;
        $message->direction = "out";
        $message->content = json_encode($result);
        $message->metadata = null;
        $message->channel = "webchat";
        $message->type = "object";
        $message->message_time = date("Y-m-d h:i:s");

        if($message->content != "{}"){
            $message->save();
        }
    }
}
