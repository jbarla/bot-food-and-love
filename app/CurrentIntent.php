<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentIntent extends Model
{
    protected $table = "current_intent";

    public static function getCurrentIntent($session_id){
        $currentIntent = CurrentIntent::where("session_id", $session_id)->first();
        return $currentIntent;
    }

    public static function setCurrentIntent($session_id, $intent){
        if($session_id && $intent){
                $del = CurrentIntent::where('session_id',$session_id)->delete();

                $currentIntent = new CurrentIntent();
                $currentIntent->session_id = $session_id;
                $currentIntent->interaction = $intent;
                return $currentIntent->save();
        }

        return false;
    }

}
