<?php

namespace App\Modules\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = "ecommerce_cart";

    public function items()
    {
        return $this->hasMany('App\Modules\Ecommerce\Models\CartItems');
    }
}
