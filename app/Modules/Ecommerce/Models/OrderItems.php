<?php

namespace App\Modules\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $table = "ecommerce_order_items";

    public function order()
    {
        return $this->hasOne('App\Modules\Ecommerce\Models\Order');
    }
}
