<?php

namespace App\Modules\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "ecommerce_order";

    public function items()
    {
        return $this->hasMany('App\Modules\Ecommerce\Models\OrderItems');
    }

}
