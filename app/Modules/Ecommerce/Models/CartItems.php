<?php

namespace App\Modules\Ecommerce\Models;

use Illuminate\Database\Eloquent\Model;

class CartItems extends Model
{
    protected $table = "ecommerce_cart_items";

    public function cart()
    {
        //return $this->belongsTo('Selene\Modules\Ecommerce\Models\Cart');
        return $this->hasOne('App\Modules\Ecommerce\Models\Cart');
    }
}
