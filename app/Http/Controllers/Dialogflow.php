<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google\Cloud\Dialogflow\V2\SessionsClient;
use Google\Cloud\Dialogflow\V2\TextInput;
use Google\Cloud\Dialogflow\V2\QueryInput;
class Dialogflow extends Controller
{
    const PROJECT_ID = "p-ch-8-kqpxip";

    public $operatorIntent;

    public $languageCode;

    public $channel;

    public function __construct($config = [
        "operatorIntent" => "Saludar",
        "languageCode" => 'en-US'
    ])
    {
        $this->channel = "whatsapp";

        if(isset($config["operatorIntent"]) && $config["operatorIntent"]){
            $this->operatorIntent = $config["operatorIntent"];
        }

        if(isset($config["languageCode"]) && $config["languageCode"]){
            $this->languageCode = $config["languageCode"];
        }

    }


    //detect_intent_texts
    public function sendMessageDialogflow($url_dialogflow_auth_json, $text, $sessionId)
    {
        // new session
        $test = array('credentials' => $url_dialogflow_auth_json);

        $sessionsClient = new SessionsClient($test);
        $session = $sessionsClient->sessionName(self::PROJECT_ID, $sessionId ?: uniqid());

        // query for each string in array
        $textInput = new TextInput();
        $textInput->setText($text);
        $textInput->setLanguageCode($this->languageCode);

        // create query input
        $queryInput = new QueryInput();
        $queryInput->setText($textInput);

        // get response and relevant info
        $response = $sessionsClient->detectIntent($session, $queryInput);

        $sessionsClient->close();
        return $response;
    }

    public function getBotPathByName($botName, $dialogflowJson){
        $bn = $botName;

        if(strpos($botName, " ") !== false){
            $bn = "";
            $tmp = explode(" ", $botName);
            foreach($tmp as $text){
                $bn .= ucwords($text);
            }
        }

        $path = storage_path("bots") . DIRECTORY_SEPARATOR . $bn . DIRECTORY_SEPARATOR . $dialogflowJson;
        return $path;
    }
}