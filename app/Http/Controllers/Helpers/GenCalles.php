<?php

namespace App\Http\Controllers\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GenCalles extends Controller
{
    public $xmlFiles;
    public $xmlFile;
    public $names;

    public function index(){
        $xmlFiles = "";
        $ficheros  = scandir(base_path() . DIRECTORY_SEPARATOR ."gencalles", 1);
        foreach($ficheros as $fichero){
            if($fichero != "." && $fichero != ".." && $fichero != "desktop.ini"){
                $this->gen($fichero);
            }
        }

        $this->names = array_unique($this->names);

        $this->outputCsv();
        /*if(is_file(base_path() . DIRECTORY_SEPARATOR . "p-ch-8-kqpxip-650a41e2b6e3.json")){
            $dialogflow_key_path = base_path() . DIRECTORY_SEPARATOR . "p-ch-8-kqpxip-650a41e2b6e3.json";
        }*/
    }

    public function outputCsv(){
        $fp = fopen(base_path() . DIRECTORY_SEPARATOR . 'records.csv', 'w');
        foreach($this->names as $name){
            $tmp = ["calles", $name];
            fputcsv($fp, $tmp);
        }

        fclose($fp);
    }

    public function makeSinonimos(){

    }

    public function gen($fichero){

        $xmlstring = base_path() . DIRECTORY_SEPARATOR ."gencalles" . DIRECTORY_SEPARATOR . $fichero;
        $xml = simplexml_load_string(file_get_contents($xmlstring));
        foreach($xml->node as $node){
            if(isset($node->tag) && $node->tag){
                foreach($node->tag as $tag){
                    if(isset($tag->attributes()["k"]) && $tag->attributes()["k"]){
                        if($tag->attributes()["k"] == "addr:street"){
                            if(isset($tag->attributes()["v"]) && $tag->attributes()["v"]){
                                $name = $tag->attributes()["v"];
                                $name = (array) $name;
                                $this->names[] = $name[0];
                            }
                        }
                    }
                }
            }
        }
    }
}
