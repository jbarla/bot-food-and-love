<?php

namespace App\Http\Controllers\Bot\_1_0_1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Bot\_1_0_1\traits\Ecommerce;
use App\Http\Controllers\Bot\_1_0_1\traits\SessionTrait;
use App\Http\Controllers\Bot\_1_0_1\traits\ResponcesTrait;
use App\Http\Controllers\Bot\_1_0_1\traits\ChannelTranslate;
use Illuminate\Http\Response;

use App\CurrentIntent;
use App\Session;
use App\SessionData;
use App\Block;
use Log;
use App\Http\Controllers\Bot\_1_0_1\traits\NlpTrait;

class IndexController extends Controller
{
    use Ecommerce, SessionTrait, ResponcesTrait, NlpTrait;

    const LOCAL_NAMESERVER = "foodlove.local";

    public $from;
    public $to;
    public $message;
    public $optValid;
    public $maxInvalididIntents;
    public $block;
    public $watsonApiUrl;
    public $watsonApiKey;

    public function __construct()
    {
        $this->query = "";
        $this->watsonApiUrl = "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/9cf04a17-361c-4019-b85c-04f17bace660/message?version=2019-09-16";
        $this->watsonApiKey = "O-n64PPdxUIuDHAI07B5Da8NqxxjUbvW7t7JVTRKzUbD";
        $this->maxInvalididIntents = 30;
        $this->company_name = "foodAndLove";
    }

    public function reciveFromWatson(Request $request){
        $params = $request->all();

        $session_id = explode(".", $params["session_id"]);

        $this->from = $session_id[0];
        $this->to = $session_id[1];
        $this->query = $params["query"];

        $this->session = $this->getSession();

        $results = $this->reciveWatson($params, $this);
        if(isset($results["messages"]) && $results["messages"]){
            foreach($results["messages"] as $result){
                if($result["type"] == "text"){
                    $reply = ChannelTranslate::sendText($result["message"]);
                    $this->setResponce($reply);
                }elseif($result["type"] == "card"){
                    //$reply = ChannelTranslate::sendQuickReplies($result["message"]["title"], $result["message"]["messages"]);
                    //Para watson
                    $reply = ChannelTranslate::sendCard($result["title"], $result["uri"]);
                    $this->setResponce($reply);
                }
            }
        }

        $this->renderResponce();

        $this->unblock();

        return response()->json(['status' => 'ok'])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }

    public function behaviorNlp($request){
        $request = $request->all();
        $this->setNlpEngine("watson");
        $results = $this->runNlp($request);

        if(isset($results["messages"]) && $results["messages"]){
            foreach($results["messages"] as $result){
                if($result["type"] == "text"){
                    $reply = ChannelTranslate::sendText($result["message"]);
                    $this->setResponce($reply);
                }elseif($result["type"] == "quick_replies"){
                    $reply = ChannelTranslate::sendQuickReplies($result["message"]["title"], $result["message"]["messages"]);
                    $this->setResponce($reply);
                }
            }
        }
    }

    public function behaviorIvr(){
        switch ($this->currentIntent->interaction) {
            case "welcome":
                $this->welcome();
                $this->mainMenu();
                break;
            case "mainMenu":
                if ($this->query == "1") {
                    $this->listCategories();
                } elseif ($this->query == "2") {
                    $this->statusOrder();
                } elseif ($this->query == "4") {
                    $this->close();
                } elseif ($this->query == "0") {
                    $this->mainMenu();
                } else {
                    $this->invalidOption("mainMenu");
                }
                break;
            case "statusOrder":
                if ($this->query == "0") {
                    $this->mainMenu();
                } else {
                    $this->invalidOption("statusOrder");
                }
                break;
            case "listCategories":
                if (!$this->validateQuery(false, null, false)) break;

                if ($this->query > "0" && $this->query <= count($this->categories)) {
                    $this->listItems($this->query);
                } else {
                    $this->invalidOption("listItems");
                }
                break;
            case "listItems":
                if (!$this->validateQuery("mainMenu", null, true)) break;

                if ($this->query > "0" && $this->query <= count($this->categories)) {
                    $this->listItems($this->query);
                } else {
                    $this->invalidOption("listItems");
                }
                break;
            case "showItem":
                if (!$this->validateQuery("listCategories", null, true)) break;

                if ($this->query > "0" && (isset($this->categories[$this->query]) && $this->query <= count($this->categories[$this->query]))) {
                    $this->showItem($this->query);
                } else {
                    $this->invalidOption("listItems");
                }

                break;
            case "menuItem":
                if (!$this->validateQuery("listItems", null, true)) break;

                if ($this->query === "1") {
                    $this->addToCart($this->query);
                } else {
                    $this->invalidOption("showItem");
                }

                break;
            case "addQtyItemCart":
                if (!$this->validateQuery("addQtyItemCart", null, true)) break;

                if (!is_numeric($this->query) || $this->query < 0 || $this->query > 99) {
                    $this->setResponce(ChannelTranslate::sendText("La cantidad debe ser un nùmero entre 1 y 99"));
                    $this->invalidOption("addQtyItemCart");
                } else {
                    $this->addQtyItemCart($this->query);
                }

                break;
            case "addExtraQption":

                if (!is_numeric($this->query)) {
                    $this->invalidOption("addExtraQption");
                } else {
                    $this->addExtraQption($this->query);
                }

                break;
            case "menuItemAddedCart":
                if (!$this->validateQuery("listCategories", null, true)) break;
                $this->menuItemAddedCart($this->query);
                break;
            case "showCart":
                if (!$this->validateQuery("listCategories", null, true)) break;

                if ($this->query == "1") {
                    $this->startOrder();
                } elseif ($this->query == "2") {
                    $this->menuDeleteItemCart();
                } else {
                    $this->invalidOption("showCart");
                }
                break;
            case "menuDeleteItemCart":
                $this->menuDeleteItemCart($this->query);
                break;
            case "setAddress":
                $this->setAddress($this->query);
                break;
            case "setkPaymentMethod":
                $this->setkPaymentMethod($this->query);
                break;
            case "askConfirmOrder":
                if ($this->query == "1") {
                    $this->set("confirmOrder", "true");
                    $this->startOrder();
                } elseif ($this->query == "2") {
                    $this->cancelOrder();
                } else {
                    $this->invalidOption("askConfirmOrder");
                }
                break;
            default:
                break;
        }
    }

    public function init($request)
    {

        $request = $request->all();
        $this->message = "";

        $this->initEcommerce();

        $this->from = $request["from"];
        $this->to = $request["to"];

        $this->session = $this->getSession();
        if (!$this->session) {
            $this->session = $this->setSession();
        }

        $this->block = Block::where("session_id", $this->session->session_id)->first();

        if(isset($request["data"]) && isset($request["data"]["type"])){
            if ($request["data"]["type"] == "chat") {
                $this->query = $request["data"]["body"];
            } elseif ($request["data"]["type"] == "image") {
                $this->query = $request["data"]["body"];
            }
        }

        $currentIntent = CurrentIntent::getCurrentIntent($this->session->session_id);

        if (!$currentIntent) {
            CurrentIntent::setCurrentIntent($this->session->session_id, "welcome");
            $currentIntent = CurrentIntent::getCurrentIntent($this->session->session_id);
        }


        $this->currentIntent = $currentIntent;

    }

    public function block()
    {
        $this->block = new Block();
        $this->block->session_id = $this->session->session_id;
        $this->block->save();
    }

    public function unblock()
    {
        Log::info($this->session->session_id);
        Block::where("session_id", $this->session->session_id)->delete();
    }

    public function index(Request $request)
    {
        $this->optValid = true;
        $this->init($request);

        if ($this->query == "-END") {
            $this->unblock();
            $this->close();
            return response()->json(['status' => 'ok'])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        } elseif ($this->query == "-UNLOCK") {
            $this->unblock();
            return response()->json(['status' => 'ok'])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        }

        if (($_SERVER["HTTP_HOST"] != self::LOCAL_NAMESERVER) && $this->block) {
            return response()->json(['status' => 'bussy'])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        }

        $this->block();

        if ($this->to == "59898843523") {
            $this->behaviorNlp($request);
        } else {
            $this->behaviorIvr();
        }

        if ($this->optValid) {
            $this->set("invalid", 0);
        }

        $this->renderResponce();

        $this->unblock();

        return response()->json(['status' => 'ok'])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }

    public function cancelOrder()
    {
        $currentIntent = CurrentIntent::where("session_id", $this->session->session_id)->delete();
        $SessionData = SessionData::where("session_id", $this->session->session_id)->delete();

        $this->message .= "*Orden cancelada*\n\n";

        $this->setResponce(
            ChannelTranslate::sendText($this->message)
        );

        $this->mainMenu();
    }

    public function statusOrder()
    {
        $orders = $this->getStatusOrder();

        if ($orders) {
            $this->message .= "*Ordenes*\n\n";
            foreach ($orders as $order) {
                $this->message .= "*------------*\n\n";
                foreach ($order->items()->get() as $item) {
                    $extraPrice = "";
                    if ($item->attributeSelected) {
                        $attribute = $this->getAttributeBySku($item->item_sku, $item->attributeSelected);
                        if ($attribute["price"]) {
                            $extraPrice = "(" . $attribute["name"] . " + $" . $attribute["price"] . ")";
                        } else {
                            $extraPrice = "(" . $attribute["name"] . ")";
                        }
                    }
                    $this->message .= $item["name"] . " x " . $item["qty"] . " $" . $item["price"] . " " . $extraPrice . "\n";
                }


                $this->message .= "\n";
                $this->message .= "*Total*\n";
                $this->message .= "$" . $order->total . "\n";
            }


        } else {
            $this->message .= "*No hay ordenes pendientes*\n\n";
        }

        $this->addOptions(false);

        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);
        CurrentIntent::setCurrentIntent($this->session->session_id, "statusOrder");
    }

    public function welcome()
    {
        $reply = ChannelTranslate::sendText("Bienvenido a Food And Love.");
        $this->setResponce($reply);
    }

    public function mainMenu()
    {
        $reply = ChannelTranslate::sendText(
            "*Elige una de las siguientes opciones*:\n" .
            "*1* - Hacer una orden\n" .
            "*2* - Consultar mi pedido\n" .
            "*3* - Hablar con operador\n" .
            "*4* - Salir\n"
        );

        $this->setResponce($reply);
        CurrentIntent::setCurrentIntent($this->session->session_id, "mainMenu");
    }

    public function close($message = "", $end = false)
    {
        $this->deleteServerSession($this->session->session_id);
        $currentIntent = CurrentIntent::where("session_id", $this->session->session_id)->delete();
        $session = Session::where("session_id", $this->session->session_id)->delete();
        $SessionData = SessionData::where("session_id", $this->session->session_id)->delete();

        if ($end) {
            $this->responce["responces"] = [];
        }

        if ($message) {
            $reply = ChannelTranslate::sendText($message);
            $this->setResponce($reply);
        }

        $reply = ChannelTranslate::sendText("Gracias por comunicarse");
        $this->setResponce($reply);
    }

    public function listCategories()
    {
        $this->set("attributeSelected", null);
        $this->set("qty", null);
        $this->set("category", null);
        $this->set("itemId", null);

        $this->message .= "*Seleccione una opciòn*\n";
        foreach ($this->categories as $category) {
            $this->message .= "*" . $category["id"] . "*" . " - " . $category["name"] . "\n";
        }

        $this->addOptions(false);

        $reply = ChannelTranslate::sendText($this->message);

        $this->setResponce($reply);
        CurrentIntent::setCurrentIntent($this->session->session_id, "listItems");
    }

    public function listItems($categoryId = null)
    {

        $this->set("attributeSelected", null);
        $this->set("qty", null);

        if ($categoryId) {
            $this->set("category", $categoryId);
        } else {
            $categoryId = $this->get("category");
        }

        foreach ($this->categories as $category) {
            if ($category["id"] == $categoryId) {
                if (isset($category["items"]) && $category["items"]) {
                    foreach ($category["items"] as $item) {
                        $this->message .= "*" . $item["id"] . "* - " . $item["name"] . " $" . $item["price"] . "\n";
                    }
                } else {
                    $this->message .= "No hay articulos.\n";
                }
            }
        }

        $this->addOptions();

        $reply = ChannelTranslate::sendText($this->message);

        $this->setResponce($reply);
        CurrentIntent::setCurrentIntent($this->session->session_id, "showItem");
    }

    public function showItem($itemId = null)
    {
        $categoryId = $this->get("category");

        $uri = "";

        if ($itemId) {
            $this->set("itemId", $itemId);
        } else {
            $itemId = $this->get("itemId");
        }

        foreach ($this->categories as $category) {
            if ($category["id"] == $categoryId) {
                foreach ($category["items"] as $item) {
                    if ($item["id"] == $itemId) {
                        $this->message .= "*" . $item["name"] . "* $" . $item["price"] . "\n";
                        $this->message .= "" . $item["description"] . "\n";
                        $uri = $item["image"];
                        break;
                    }
                }
            }
        }

        $this->addOptions(true, ["1" => "Agregar al carrito"]);

        $reply = ChannelTranslate::sendCard($this->message, $uri);

        $this->setResponce($reply);
        CurrentIntent::setCurrentIntent($this->session->session_id, "menuItem");
    }

    public function addToCart($itemId)
    {
        $sku = null;
        $categoryId = $this->get("category");
        $selectedItem = null;
        foreach ($this->categories as $category) {
            if ($category["id"] == $categoryId) {
                foreach ($category["items"] as $item) {
                    if ($item["id"] == $itemId) {
                        $sku = $item["sku"];
                        $selectedItem = $item;
                        break;
                    }
                }
            }
        }

        $this->set("sku", $sku);
        $this->message .= "*Ingrese la cantidad*\n";
        $this->addOptions();

        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);


        CurrentIntent::setCurrentIntent($this->session->session_id, "addQtyItemCart");
    }

    function showMenuExtraQption()
    {
        $sku = $this->get("sku");
        $item = $this->getItemBySku($sku);

        foreach ($item["options"] as $option) {
            if ($option["required"]) {
                $this->message .= "*Seleccione " . $option["name"] . "*\n\n";
                foreach ($option["options"] as $opt) {
                    if ($opt["price"]) {
                        $this->message .= "*" . $opt["id"] . "* - " . $opt["name"] . " + $" . $opt["price"] . "\n";
                    } else {
                        $this->message .= "*" . $opt["id"] . "* - " . $opt["name"] . "\n";
                    }
                }
                break;
            }
        }

        $this->addOptions();

        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);

        CurrentIntent::setCurrentIntent($this->session->session_id, "addExtraQption");
    }

    function addExtraQption($selected = "")
    {
        if ($selected) {
            $this->set("attributeSelected", $selected);
        } else {
            $selected = $this->get("attributeSelected");
        }

        $this->addQtyItemCart();
        //CurrentIntent::setCurrentIntent($this->session->session_id, "addToCart");
    }

    function addQtyItemCart($qty = null)
    {
        $sku = $this->get("sku");
        $item = $this->getItemBySku($sku);

        if ($qty) {
            $this->set("qty", $qty);
        }

        if (isset($item["options"]) && $item["options"]) {
            $attributeSelected = $this->get("attributeSelected");
            if (!$attributeSelected) {
                return $this->showMenuExtraQption();
            }

            $item["attributeSelected"] = $attributeSelected;
        }

        $qty = $this->get("qty");

        $this->addItemToCart($item, $qty);
        $this->message .= "*Producto agregado al carrito*\n\n";
        $this->addOptions(true, ["1" => "Finalizar compra", "2" => "Ver carrito", "3" => "Seguir comprando"]);

        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);
        CurrentIntent::setCurrentIntent($this->session->session_id, "menuItemAddedCart");
    }

    public function menuItemAddedCart($opt)
    {
        if ($opt == "1") {
            $this->startOrder();
        } elseif ($opt == "2") {
            $this->showCart();
        } elseif ($opt == "3") {
            $this->listCategories();
        } elseif ($opt === "0") {
            $categoryId = $this->get("category");
            $this->listItems($categoryId);
        } elseif ($opt === "00") {
            $this->mainMenu();
        } else {
            //Error seleccione la opcion correcta
        }
    }

    public function menuDeleteItemCart($opt = null)
    {
        $cart = $this->getCart();

        $itera = 1;
        if (!$opt) {
            $this->message .= "*Seleccione articulo a eliminar*\n\n";

            foreach ($cart->items()->get() as $item) {
                $extraPrice = "";
                if ($item->attributeSelected) {
                    $attribute = $this->getAttributeBySku($item->item_sku, $item->attributeSelected);
                    if ($attribute["price"]) {
                        $extraPrice = "(" . $attribute["name"] . " + $" . $attribute["price"] . ")";
                    } else {
                        $extraPrice = "(" . $attribute["name"] . ")";
                    }
                }
                $this->message .= "*" . $itera . "* - " . $item["name"] . " x " . $item["qty"] . " $" . $item["price"] . " " . $extraPrice . "\n";
                $itera++;
            }

            $this->addOptions(true);

            $reply = ChannelTranslate::sendText($this->message);
            $this->setResponce($reply);
            CurrentIntent::setCurrentIntent($this->session->session_id, "menuDeleteItemCart");

        } else {
            foreach ($cart->items()->get() as $item) {
                if ($itera == $opt) {
                    $this->deletefromCart($opt);
                    $this->message .= "Articulo (" . $item["name"] . ") eliminado\n";
                    $reply = ChannelTranslate::sendText($this->message);
                    $this->setResponce($reply);
                    $this->message = "";
                    return $this->showCart();
                }
                $itera++;
            }

            $this->message = "Error eliminador el articulo";
            $reply = ChannelTranslate::sendText($this->message);
            $this->setResponce($reply);
            CurrentIntent::setCurrentIntent($this->session->session_id, "menuDeleteItemCart");
        }
    }

    public function showCart()
    {
        $cart = $this->getCart();

        $this->message .= "*Carrito*\n\n";
        $totalItems = count($cart->items()->get());

        if (!$totalItems) {
            $this->message = "*El carrito esta vacío.*\n";
            $this->addOptions(true);
            $reply = ChannelTranslate::sendText($this->message);
            $this->setResponce($reply);
            CurrentIntent::setCurrentIntent($this->session->session_id, "showCart");
            return true;
        }

        foreach ($cart->items()->get() as $item) {
            $extraPrice = "";
            if ($item->attributeSelected) {
                $attribute = $this->getAttributeBySku($item->item_sku, $item->attributeSelected);
                if ($attribute["price"]) {
                    $extraPrice = "(" . $attribute["name"] . " + $" . $attribute["price"] . ")";
                } else {
                    $extraPrice = "(" . $attribute["name"] . ")";
                }
            }
            $this->message .= "- " . $item["name"] . " x " . $item["qty"] . " $" . $item["price"] . " " . $extraPrice . "\n";
        }

        $this->message .= "\n";
        $this->message .= "*Total*\n";
        $this->message .= "$" . $cart->total . "\n";

        $this->addOptions(true, ["1" => "Finalizar compra", "2" => "Eliminar articulo del carrito"]);

        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);
        CurrentIntent::setCurrentIntent($this->session->session_id, "showCart");
    }

    public function startOrder()
    {
        $address = $this->get("address");
        $paymentMethod = $this->get("paymentMethod");
        $confirmOrder = $this->get("confirmOrder");

        if (!$address) {
            return $this->askAddress();
        }

        if (!$paymentMethod) {
            return $this->askPaymentMethod();
        }

        if (!$confirmOrder) {
            return $this->askConfirmOrder();
        }

        $orderSaved = $this->setOrder();
        if ($orderSaved) {
            $this->message .= "*Orden creada*\n\n";
            $this->cleanCart();
            $this->triggerOrder($orderSaved, "new");
            //$this->destroySession();
            $reply = ChannelTranslate::sendText($this->message);
            $this->setResponce($reply);
            $this->close();
            //$this->mainMenu();
        }

    }

    public function askAddress()
    {
        $this->message .= "*Ingrese su direcciòn*\n\n";
        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);

        $this->addOptions(true);
        CurrentIntent::setCurrentIntent($this->session->session_id, "setAddress");
    }

    public function setAddress($address)
    {
        $this->set("address", $address);
        $this->startOrder();
    }

    public function askPaymentMethod()
    {
        $this->message .= "*Como desea pagar?*\n\n";

        $this->message .= "*1* - Efectivo\n";
        $this->message .= "*2* - Tarjeta de credito/debito\n\n";

        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);

        $this->addOptions(true);
        CurrentIntent::setCurrentIntent($this->session->session_id, "setkPaymentMethod");
    }

    public function askConfirmOrder()
    {
        $cart = $this->getCart();

        $this->message .= "*Ultimo paso!*\n\n";

        $this->message .= "Debes confirmar tu pedido\n";

        foreach ($cart->items()->get() as $item) {
            $extraPrice = "";
            if ($item->attributeSelected) {
                $attribute = $this->getAttributeBySku($item->item_sku, $item->attributeSelected);
                if ($attribute["price"]) {
                    $extraPrice = "(" . $attribute["name"] . " + $" . $attribute["price"] . ")";
                } else {
                    $extraPrice = "(" . $attribute["name"] . ")";
                }
            }
            $this->message .= $item["name"] . " x " . $item["qty"] . " $" . $item["price"] . " " . $extraPrice . "\n";
        }

        $address = $this->get("address");

        $this->message .= "\n*Dirección:* " . $address . "\n";

        //$this->addOptions(true, ["1" => "Confirmar pedido", "2" => "Modificar pedido", "3" => "Modificar dirección"]);
        $this->addOptions(true, ["1" => "Confirmar pedido", "2" => "Cancelar pedido"]);

        $reply = ChannelTranslate::sendText($this->message);
        $this->setResponce($reply);

        CurrentIntent::setCurrentIntent($this->session->session_id, "askConfirmOrder");
    }

    public function setkPaymentMethod($paymentMethod)
    {
        $this->set("paymentMethod", $paymentMethod);
        $this->startOrder();
    }

    public function deleteServerSession($sessionId)
    {
        //$sessionId = substr($sessionId, 11, strlen($sessionId)) . "." . substr($sessionId, 0, 11);
        $url = 'https://www.wapify.uy/api/public/delete-session';

        $client = new \GuzzleHttp\Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $data = [
            "session_id" => $sessionId
        ];

        $result = $client->post($url, [
            'body' => json_encode($data)
        ]);

        return $result;
    }

    public function testql(){
        $product = $this->getProduct(57);
        $categories = $this->getCategories();
        $productsCategory = [];
        foreach($categories as $category){
            $productsCategory[$category["id"]][] = $this->getProductsByCategories($category["id"]);
        }
    }
}
