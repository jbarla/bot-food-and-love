<?php

namespace App\Http\Controllers\Bot\_1_0_1;

use App\Http\Controllers\Controller;
use Log;
use App\Http\Controllers\Bot\_1_0_1\IndexController;

class TestController extends Controller
{
    public $indexController;

    public function setRequest(){
        $this->request = [
            "from" => $this->from,
            "to" => $this->to,
            "sessionId" => $this->from . "." . $this->to,
            "data" => [
                "id" => "",
                "body" => $this->msg,
                "type" => $this->type,
                "t" => "",
                "from" => $this->from,
                "to" => $this->to,
                "self" => "in",
                "sender" => json_decode('{"id":"'.$this->from.'@c.us","pushname":"Jotapey","type":"in","isBusiness":"0","isEnterprise":"0","statusMute":"0","formattedName":"+598 91 075 294","isMe":"0","isMyContact":"0","isPSA":"0","isUser":"1","isWAContact":"1","profilePicThumbObj":{"eurl":"https:\/\/pps.whatsapp.net\/v\/t61.24694-24\/65788280_473684160123103_8357019189847785472_n.jpg?oe=5DABACA8&oh=f5a0d6b29401160ffffc4751e10fc055","id":"59891075294@c.us","img":"https:\/\/web.whatsapp.com\/pp?e=https%3A%2F%2Fpps.whatsapp.net%2Fv%2Ft61.24694-24%2F65788280_473684160123103_8357019189847785472_n.jpg%3Foe%3D5DABACA8%26oh%3Df5a0d6b29401160ffffc4751e10fc055&t=s&u=59891075294%40c.us&i=1564186201","imgFull":"https:\/\/web.whatsapp.com\/pp?e=https%3A%2F%2Fpps.whatsapp.net%2Fv%2Ft61.24694-24%2F65788280_473684160123103_8357019189847785472_n.jpg%3Foe%3D5DABACA8%26oh%3Df5a0d6b29401160ffffc4751e10fc055&t=l&u=59891075294%40c.us&i=1564186201","tag":"1564186201"}}', true),
                "chat" => json_decode('{"id":"'.$this->from.'@c.us","pendingMsgs":"0","lastReceivedKey":{"fromMe":"0","remote":"59891075294@c.us","id":"268583DA26CB2ECFC5C0753B8A64B0BE","_serialized":"false_59891075294@c.us_268583DA26CB2ECFC5C0753B8A64B0BE"},"t":"1571401216","unreadCount":"0","archive":"0","isReadOnly":"0","muteExpiration":"0","notSpam":"1","pin":"0","kind":"chat","isGroup":"0","contact":{"id":"59891075294@c.us","pushname":"Jotapey","type":"in","isBusiness":"0","isEnterprise":"0","statusMute":"0","formattedName":"+598 91 075 294","isMe":"0","isMyContact":"0","isPSA":"0","isUser":"1","isWAContact":"1","profilePicThumbObj":{"eurl":"https:\/\/pps.whatsapp.net\/v\/t61.24694-24\/65788280_473684160123103_8357019189847785472_n.jpg?oe=5DABACA8&oh=f5a0d6b29401160ffffc4751e10fc055","id":"59891075294@c.us","img":"https:\/\/web.whatsapp.com\/pp?e=https%3A%2F%2Fpps.whatsapp.net%2Fv%2Ft61.24694-24%2F65788280_473684160123103_8357019189847785472_n.jpg%3Foe%3D5DABACA8%26oh%3Df5a0d6b29401160ffffc4751e10fc055&t=s&u=59891075294%40c.us&i=1564186201","imgFull":"https:\/\/web.whatsapp.com\/pp?e=https%3A%2F%2Fpps.whatsapp.net%2Fv%2Ft61.24694-24%2F65788280_473684160123103_8357019189847785472_n.jpg%3Foe%3D5DABACA8%26oh%3Df5a0d6b29401160ffffc4751e10fc055&t=l&u=59891075294%40c.us&i=1564186201","tag":"1564186201"}},"presence":{"id":"59891075294@c.us"}}', true),
                "chatId" => $this->from."@c.us"
            ],
            "XDEBUG_SESSION_START" => "1"
        ];
    }

    public function __construct()
    {

    }

    public function index(){
        runTests();
    }

    public function runTests(){
        $this->testUno();
    }

    public function testUno(){
        $this->indexController = new IndexController();

        $this->type = "chat";
        $this->from = "59891075294";
        $this->to = "59898843523";
        $this->msg = "Hola";
        $this->setRequest();
    }

}
