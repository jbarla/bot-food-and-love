<?php

namespace App\Http\Controllers\Bot\_1_0_1\traits;

use App\Message;
use App\Http\Controllers\Nlp\ChannelTranslate;

trait ResponcesTrait
{
    public $responce = ["responces"];

    public function setResponce($responce)
    {
        $this->responce["responces"][] = $responce;
    }

    public function renderResponce()
    {
        
	$url = 'https://www.wapify.uy/api/public/send';

        $client = new \GuzzleHttp\Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $data = [
            "destination_phone" => $this->from,
            "bot_phone" => $this->to,
            "responces" => $this->responce["responces"],
        ];

        $result = $client->post($url,[
            'body' => json_encode($data)
        ]);

        return $result;
    }

    public function addOptions($mainMenu = true, $options = []){
        if($this->message){
            $this->message .= "\n";

            foreach($options as $key => $value){
                $this->message .= "*".$key."* - ".$value."\n";
            }

            $this->message .= "*0* - Volver\n";
            if($mainMenu){
                $this->message .= "*00* - Menù principal\n";
            }
        }
    }

    public function invalidOption($option, $params = ""){
        $reply = ChannelTranslate::sendText("Opcion Incorrecta.");
        $invalid = $this->get("invalid");
        $this->optValid = false;

        if(!$invalid){
            $invalid = 1;
        }else{
            $invalid++;
            if($invalid > $this->maxInvalididIntents){
                return $this->close("Supero intentos", true);
            }
        }

        $this->set("invalid", $invalid);
        $this->setResponce($reply);
        if(!$params){
            $this->$option();
        }else{
            $this->$option($params);
        }
    }

    public function validateQuery($option, $params = null,$main = true){
        if(!$option){
            if(!$this->query){
                $this->mainMenu();
                return false;
            }
        }

        if($main){
            if($this->query === "00"){
                $this->mainMenu();
                return false;
            }
        }

        if($this->query === "0"){
            if(!$params){
                $this->$option();
            }else{
                $this->$params();
            }

            return false;
        }

        return true;
    }

}
