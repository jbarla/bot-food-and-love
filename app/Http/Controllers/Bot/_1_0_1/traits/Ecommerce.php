<?php

namespace App\Http\Controllers\Bot\_1_0_1\traits;

use App\Http\Controllers\Bot\_1_0_1\traits\GraphQl;
use
    App\Modules\Ecommerce\Models\Cart,
    App\Modules\Ecommerce\Models\CartItems,
    App\Modules\Ecommerce\Models\Order,
    App\Modules\Ecommerce\Models\OrderItems;

trait Ecommerce
{
    use GraphQl;

    public $categories;
    public $secret;

    public function watsonCategories(){
        return [
            "11" => "Hamburguesas",
            "12" => "Milanesas"
        ];
    }

    public function initEcommerce()
    {
        $this->secret = "b33a1eeb13f7fc3c38b6b5278b1f48cb";
        $this->company_id = 2;
        $this->setCategoriesItems();
    }

    public function getStatusOrder()
    {
        $order = Order::where("session_id", $this->session->session_id)->where("status", "new")->get();

        return $order;
    }

    public function getCart()
    {
        $cart = Cart::where("session_id", $this->session->session_id)->first();

        if (!$cart) {
            $cart = new Cart();
            $cart->session_id = $this->session->session_id;
            $cart->total = 0;
            $cart->save();
        }

        return $cart;
    }

    public function getItemBySku($sku)
    {
        foreach ($this->categories as $category) {
            if (isset($category["items"]) && $category["items"]) {
                foreach ($category["items"] as $item) {
                    if ($item["sku"] == $sku) {
                        return $item;
                    }
                }
            }
        }

        return false;
    }

    public function addItemToCart($item, $qty)
    {
        $cart = $this->getCart();
        $alreadYadded = false;
        $totalGlobal = 0;

        foreach ($cart->items()->get() as $itemCart) {
            if ($itemCart->item_sku == $item["sku"]) {
                $newTotal = 0;
                $oldTotal = 0;

                $attributeSelected = (isset($item["attributeSelected"]) && $item["attributeSelected"]) ? $item["attributeSelected"] : null;
                if ($attributeSelected) {
                    $newTotal += $this->getAttributePrice($itemCart->item_sku, $attributeSelected);
                }

                $newTotal += $item["price"];

                if ($itemCart->attributeSelected) {
                    $oldTotal += $this->getAttributePrice($itemCart->item_sku, $itemCart->attributeSelected);
                }

                $oldTotal += $itemCart->price;

                if ($newTotal == $oldTotal) {
                    $alreadYadded = true;
                    $itemCart->qty = $itemCart->qty + $qty;
                    $totalGlobal += $itemCart->price * $qty;
                    $itemCart->save();
                }
            }
        }

        if (!$alreadYadded) {
            $totalItem = 0;
            $cartItems = new CartItems();
            $cartItems->item_sku = $item["sku"];
            $cartItems->qty = $qty;
            $cartItems->cart_id = $cart->id;
            $cartItems->name = $item["name"];
            $cartItems->price = $item["price"];
            $cartItems->attributeSelected = (isset($item["attributeSelected"]) && $item["attributeSelected"]) ? $item["attributeSelected"] : null;

            $totalItem += $cartItems->price;
            $totalItem += $this->getAttributePrice($cartItems->item_sku, $cartItems->attributeSelected);
            $totalGlobal += $totalItem * $qty;

            $cartItems->save();
        }

        $cart->total = $this->updateCartTotal();

        $cart->save();
    }

    public function updateCartTotal()
    {
        $cart = $this->getCart();

        $totalGlobal = 0;

        foreach ($cart->items()->get() as $item) {
            $totalItem = 0;
            $extraPrice = "";
            if ($item->attributeSelected) {
                $attribute = $this->getAttributeBySku($item->item_sku, $item->attributeSelected);
                if ($attribute["price"]) {
                    $totalItem += $attribute["price"];
                }
            }
            $totalItem += $item->price;
            $totalItem = $totalItem * $item->qty;
            $totalGlobal += $totalItem;
        }

        return $totalGlobal;
    }

    public function deletefromCart($pos)
    {
        $cart = $this->getCart();
        $itera = 1;
        foreach ($cart->items()->get() as $itemCart) {
            if ($pos == $itera) {
                $itemCart->delete();
                break;
            }
            $itera++;
        }

        $cart->total = $this->updateCartTotal();

        $cart->save();
    }

    public function setAddress()
    {

    }

    public function getAttributePrice($sku, $attrSelected)
    {
        foreach ($this->categories as $category) {
            if (isset($category["items"]) && $category["items"]) {
                foreach ($category["items"] as $item) {
                    if ($item["sku"] == $sku) {
                        if (isset($item["options"]) && $item["options"]) {
                            foreach ($item["options"] as $option) {
                                if (isset($option["options"]) && $option["options"]) {
                                    foreach ($option["options"] as $attribute) {
                                        if ($attribute["id"] == $attrSelected) {
                                            return $attribute["price"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function getAttributeBySku($sku, $attrSelected)
    {
        foreach ($this->categories as $category) {
            if (isset($category["items"]) && $category["items"]) {
                foreach ($category["items"] as $item) {
                    if ($item["sku"] == $sku) {
                        if (isset($item["options"]) && $item["options"]) {
                            foreach ($item["options"] as $option) {
                                if (isset($option["options"]) && $option["options"]) {
                                    foreach ($option["options"] as $attribute) {
                                        if ($attribute["id"] == $attrSelected) {
                                            return $attribute;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function setOrder()
    {
        $cart = $this->getCart();

        $order = new Order();
        $order->session_id = $this->session->session_id;
        $order->total = 0;
        $order->company_id = $this->company_id;
        $order->shopping_address = $this->get("address");
        $order->payment_method = $this->get("paymentMethod");
        $order->contact_phone = $this->from;
        $order->status = "new";
        $order->save();


        $total = 0;

        foreach ($cart->items()->get() as $itemCart) {
            $orderItem = new OrderItems();

            $orderItem->item_sku = $itemCart->item_sku;
            $orderItem->attributeSelected = $itemCart->attributeSelected;
            $orderItem->qty = $itemCart->qty;
            $orderItem->name = $itemCart->name;
            $orderItem->price = $itemCart->price;
            /*$total += $orderItem->price;
            $total += $this->getAttributePrice($orderItem->item_sku, $orderItem->attributeSelected);*/
            $orderItem->order_id = $order->id;
            $orderItem->save();
        }

        $order->total = $cart->total;
        $order->status = "new";
        $order->save();
        return $order;
    }

    public function cleanCart()
    {
        Cart::where("session_id", $this->session->session_id)->delete();
    }

    public function triggerOrder($order, $mode = "new")
    {

    }

    public function getCategories($token){
        $query = <<<'GRAPHQL'
{
 collections{
   items{
     name,
     id
   }
 }
}
GRAPHQL;
        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        //return $result["data"]["collections"]["items"];
        return json_decode($result["data"], true)["data"]["collections"]["items"];
    }

    public function getProductsByCategories($category, $token){
        $query = "";
        $query .="{";
        $query .=" search(input: {groupByProduct: true,collectionId:{$category},";
        $query .="}) {";
        $query .="           items {";
        $query .="               productId";
        $query .="               slug";
        $query .="               productName";
        $query .="               description";
        $query .="               priceWithTax {";
        $query .="                   ... on PriceRange {";
        $query .="                       min";
        $query .="                       max";
        $query .="                   }";
        $query .="               }";
        $query .="               productPreview";
        $query .="           }";
        $query .="           totalItems";
        $query .="           facetValues {";
        $query .="               count";
        $query .="               facetValue {";
        $query .="                   id";
        $query .="                   name";
        $query .="                   facet {";
        $query .="                       id";
        $query .="                       name";
        $query .="                   }";
        $query .="               }";
        $query .="           }";
        $query .="       }";
        $query .="}";

        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        //return $result["data"]["search"]["items"];
        return json_decode($result["data"], true)["data"]["search"]["items"];
    }

    public function getProduct($id, $token){
        $query = "";
        $query .= "{";
        $query .= " product(id:{$id}) {";
        $query .= "     id";
        $query .= "     name";
        $query .= "     description";
        $query .= "     variants {";
        $query .= "         id";
        $query .= "         name";
        $query .= "         options {";
        $query .= "             id";
        $query .= "             code";
        $query .= "             name";
        $query .= "                 groupId";
        $query .= "         }";
        $query .= "         price";
        $query .= "         priceWithTax";
        $query .= "         sku";
        $query .= "     }";
        $query .= "     assets {";
        $query .= "         id";
        $query .= "         name";
        $query .= "         preview";
        $query .= "         type";
        $query .= "     },";
        $query .= "         optionGroups{";
        $query .= "     id,";
        $query .= "     name";
        $query .= "     }";
        $query .= " }";
        $query .= "}";
        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        return json_decode($result["data"], true)["data"]["product"];
    }

    public function addItemToOrder($variantId, $quantity, $token){
        $query = "";
        $query .= "mutation {";
        $query .= "   addItemToOrder(productVariantId:{$variantId}, quantity:{$quantity}){";
        $query .= "     id";
        $query .= "   }";
        $query .= " }";
        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        return $result;
    }

    public function activeOrder($variantId, $quantity, $token){
        $query = "";
        $query .= "mutation {";
        $query .= "   addItemToOrder(productVariantId:{$variantId}, quantity:{$quantity}){";
        $query .= "     id";
        $query .= "   }";
        $query .= " }";
        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        return $result;
    }

    public function setOrderShippingMethod($token){
        $query = "";
        $query .= "mutation{";
        $query .= " setOrderShippingMethod(shippingMethodId:1){";
        $query .= "   id";
        $query .= " }";
        $query .= "}";
        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        return $result;
    }

    public function setOrderShippingAddress($address, $orderId, $phone, $token){
        $query = "";
        $query .= 'mutation{';
        $query .= ' setOrderShippingAddress(input:{';
        $query .= '   fullName: "Default",';
        $query .= '   streetLine1:"'.$address.'",';
        $query .= '   streetLine2:"",';
        $query .= '   countryCode:"UY",';
        $query .= '   city:"Montevideo",';
        $query .= '   phoneNumber:"'.$phone.'"';
        $query .= " }){";
        $query .= "   id,";
        $query .= "   state";
        $query .= "   shippingAddress{";
        $query .= "     fullName";
        $query .= "   }";
        $query .= " }";
        $query .= "}";
        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        return $result;
    }

    public function setCustomerForOrder($phone, $token){
        $query = '';
        $query .= 'mutation{';
        $query .= ' setCustomerForOrder(input:{';
        $query .= '   firstName: "default",';
        $query .= '   lastName:"default",';
        $query .= '   phoneNumber:"'.$phone.'",';
        $query .= '   emailAddress:"default@gmail.com",';
        $query .= '   customFields:{';
        $query .= '     name:"Default",';
        $query .= '     surname: "Default"';
        $query .= '   }';
        $query .= ' }){';
        $query .= '   id,';
        $query .= '   state,';
        $query .= ' }';
        $query .= '}';
        $result = GraphQl::query('http://52.39.223.60/shop-api', $query, ['user' => 'dunglas'], $token);
        return $result;
    }


    public function setCategoriesItems($categories = false)
    {

        if($categories){
            $this->categories = $categories;
            return true;
        }

        $this->categories = [
            "categories" => [
                "1" => [
                    "id" => "1",
                    "name" => "PICADAS",
                    "items" => [
                        [
                            "sku" => "1001",
                            "id" => "1",
                            "name" => "Picada Food & Love",
                            "watson_id" => "Picada Food & Love",
                            "description" => "Milanesas de carne y pollo (milanesas picadas), aros de cebolla, papas fritas, milanesa de jamón y queso fritos, aderezos, arrolladitos primavera, y mitad muzzarella.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/a67b7084-bc21-4c7b-8c97-77ca71fc6404.jpg",
                            "price" => "871",
                            "options_group" => [1],
                        ],
                        [
                            "sku" => "1002",
                            "id" => "2",
                            "name" => "Papas champi",
                            "watson_id" => "Papas champi",
                            "description" => "",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/ed09353b-f80a-44d0-bb26-0af4a3b37007.jpg",
                            "price" => "312",
                            "options_group" => [1]
                        ],
                        [
                            "sku" => "1003",
                            "id" => "3",
                            "name" => "Papas Food & Love",
                            "watson_id" => "Papas Food & Love",
                            "description" => "Salsa en base a cheddar y panceta bacon.",
                            "image" => "",
                            "price" => "312",
                        ],
                        [
                            "sku" => "1004",
                            "id" => "4",
                            "name" => "Papas Mex",
                            "watson_id" => "Papas Mex",
                            "description" => "Con Salsa de tomate picante",
                            "image" => "",
                            "price" => "312"
                        ],
                        [
                            "sku" => "1005",
                            "id" => "5",
                            "name" => "Papas Barbacoa",
                            "watson_id" => "Papas Barbacoa",
                            "description" => "Con salsa barbacoa casera",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/a7ee661e-be25-41a7-a5a6-199791f24fa6.jpg",
                            "price" => "871",
                            "options_group" => [1]
                        ],
                        [
                            "sku" => "1006",
                            "id" => "6",
                            "name" => "Papas caseras",
                            "watson_id" => "Papas caseras",
                            "description" => "Papas cortadas y peladas a mano",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/38bbbfae-2793-4efc-b204-dfd0f500e536.jpg",
                            "price" => "180"
                        ],
                        [
                            "sku" => "1007",
                            "id" => "7",
                            "name" => "Aros de cebolla",
                            "watson_id" => "Aros de cebolla",
                            "description" => "16 aros de cebolla",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/6708315e-99c9-4614-a9b1-b8e44efba3c5.jpg",
                            "price" => "200"
                        ]
                    ]
                ],
                "2" => [
                    "id" => "2",
                    "name" => "BURGERS GOURMET",
                    "items" => [
                        [
                            "sku" => "2001",
                            "id" => "8",
                            "name" => "Burger Shake & Shark NUEVA!",
                            "watson_id" => "Burger Shake & Shark",
                            "description" => "Lechuga,tomate, doble cheddar, queso crema, alioli, en delicioso pan tortuga artesanal",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/cdb972f3-c590-42dc-9bf2-b37c6b3cdc75_d3.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2002",
                            "id" => "9",
                            "name" => "Burger Elevator",
                            "watson_id" => "Burger Elevator",
                            "description" => "200 g de carne en pan casero, doble cheddar, bacon , huevo frito y salsa BBQ.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/401940ed-bf37-46f9-ba4f-f075d1c60203.jpg",
                            "price" => "410",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2003",
                            "id" => "10",
                            "name" => "Burger Sweet Love",
                            "watson_id" => "Burger Sweet Love",
                            "description" => "200 g de hamburguesa en pan tortuga casera con doble sésamo, cebolla caramelizada, panceta bacon y cheddar.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/0682fd94-d9ba-44ce-9c9f-094209f992b0.jpg",
                            "price" => "361",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2004",
                            "id" => "11",
                            "name" => "Burger doble Sweet Love",
                            "watson_id" => "Burger doble Sweet Love",
                            "description" => "Doble hamburguesa en pan tortuga casera con doble sésamo, cebolla caramelizada, panceta bacon y cheddar.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/c2bba509-c828-4cbc-84f8-c1da1669ebaf.jpg",
                            "price" => "410",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2005",
                            "id" => "12",
                            "name" => "Burger Veggie",
                            "watson_id" => "Burger Veggie",
                            "description" => "Hamburguesa casera de soja en pan tortuga casera con doble sésamo, lehuga crespa, tomate en cubo, colchon de rúcula y queso crema saborizado.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/14fc7988-5782-4a84-b4cc-0d97eca58bc7.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2006",
                            "id" => "13",
                            "name" => "Burger Power Veggie",
                            "watson_id" => "Burger Power Veggie",
                            "description" => "Hamburguesa de soja casera, en pan tortuga casera con doble sésamo, lechuga crespa, huevo frito, queso cheddar y tomate en cubos.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/14fc7988-5782-4a84-b4cc-0d97eca58bc7.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2007",
                            "id" => "14",
                            "name" => "Burger Dallas",
                            "watson_id" => "Burger Dallas",
                            "description" => "200 g de hamburguesa en pan tortuga casera con doble sésamo, 2 aros de cebolla fritos, panceta bacon, cheddar y salsa bbq casera.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/f25958b6-cb6e-40cf-8f02-015830040386.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2008",
                            "id" => "15",
                            "name" => "Burger Dijón",
                            "watson_id" => "Burger Dijón",
                            "description" => "200 g de hamburguesa casera en pan de sésamo, rúcula, mostaza dijón y variedad de quesos.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/d1099346-743d-4eb8-82d1-8409969bdf4c.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2009",
                            "id" => "16",
                            "name" => "Burger Clásica",
                            "watson_id" => "Burger Clásica",
                            "description" => "Hamburguesa de 200 g en pan tortuga casera con doble sésamo, lechuga, tomate y mayonesa.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/277b86c2-579a-40b4-a358-05479fd90d3b.jpg",
                            "price" => "330",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2010",
                            "id" => "17",
                            "name" => "Burger Power BBQ",
                            "watson_id" => "Burger Power BBQ",
                            "description" => "200 g de hamburguesa en pan tortuga casera con doble sésamo, bbq, cebolla, tomate, panceta bacon y huevo frito.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/277b86c2-579a-40b4-a358-05479fd90d3b.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2011",
                            "id" => "18",
                            "name" => "Burger Big Love",
                            "watson_id" => "Burger Big Love",
                            "description" => "200 g de hamburguesa casera en pan tortuga con doble sésamo, mayonesa saborizada, pepinillos, cebolla y lechuga.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/391f9cc2-7b29-49e5-acb1-c0417b4780f9_d2.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2012",
                            "id" => "19",
                            "name" => "Burger Salad Mix",
                            "watson_id" => "Burger Salad Mix",
                            "description" => "200 g de hamburguesa en pan tortuga casera con doble sésamo, tomate, lechuga, cebolla morada, cheddar fundido, mayonesa.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/0f408f0a-b6ee-4cbe-8862-d4289873daaf.jpg",
                            "price" => "360",
                            "options_group" => [2, 3],
                        ],
                        [
                            "sku" => "2013",
                            "id" => "20",
                            "name" => "Burger Four Cheese",
                            "watson_id" => "Burger Four Cheese",
                            "description" => "200 gr de hamburguesa en pan tortuga casera con doble sésamo, lascas de parmesano, roquefort, muzzarella y queso crema.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/fb5f05c8-47cc-44e8-b89e-44b5e87704ef.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2014",
                            "id" => "21",
                            "name" => "Burger Ibérica",
                            "watson_id" => "Burger Ibérica",
                            "description" => "200 gr de hamburguesa en pan tortuga casera con doble sésamo, jamón crudo, colchón de rúcula y muzzarella.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/a6774feb-91d2-482a-b1ee-ac3915028912.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2015",
                            "id" => "22",
                            "name" => "Burger 1/4 libra",
                            "watson_id" => "Burger 1/4 libra",
                            "description" => "200g de hamburguesa casera, en pan casero , doble queso cheddar, ketchup y cebolla colorada.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/0ea6a1a6-8413-475e-9995-470d4e91444c.jpg",
                            "price" => "330",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2016",
                            "id" => "23",
                            "name" => "Burger ByPass Love",
                            "watson_id" => "Burger ByPass Love",
                            "description" => "200 g de carne en pan tortuga casera con doble sésamo, triple cheddar, doble panceta, doble muzarella, cebolla y salsa BBQ casera.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/324e4749-b897-456b-8bdc-fcd2b5bd6af4.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2017",
                            "id" => "24",
                            "name" => "Doble Burger ByPass Love .",
                            "watson_id" => "Doble Burger ByPass Love",
                            "description" => "400g de carne en pan tortuga casera con doble sésamo, cuádruple cheddar, triple de panceta bacon, doble muzarella, cebolla colorada y salsa BBQ casera",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/7db23d00-3331-407f-b57a-df30e7d080ec.jpg",
                            "price" => "410",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2018",
                            "id" => "25",
                            "name" => "Burger Blue Cheese",
                            "watson_id" => "Burger Blue Cheese",
                            "description" => "Pan casero, 200g de carne, cebolla caramelizada, rucula, muzarella y roquefort.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/9dfa14fb-2a0f-4dac-ba31-eb37096a8a1a.jpg",
                            "price" => "360",
                            "options_group" => [2, 3]
                        ],
                        [
                            "sku" => "2019",
                            "id" => "26",
                            "name" => "Burger Doble Elevator",
                            "watson_id" => "Burger Doble Elevator",
                            "description" => "400g de carne en pan casero, doble hamburguesa, doble cheddar, doble bacon, dos huevos fritos y salsa BBQ casera",
                            "image" => "",
                            "price" => "410",
                            "options_group" => [2, 3]
                        ],
                    ]
                ],
                "3" => [
                    "id" => "3",
                    "name" => "MILANESAS GOURMET XL",
                    "items" => [
                        [
                            "sku" => "3001",
                            "id" => "27",
                            "name" => "Milanesa classic con guarnición",
                            "watson_id" => "Milanesa classic con guarnición",
                            "description" => "",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/5a954324-71c0-4d9e-9ecf-ceb1c8e24b69.jpg",
                            "price" => "333",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3002",
                            "id" => "28",
                            "name" => "Milanesa canadiense",
                            "watson_id" => "Milanesa canadiense",
                            "description" => "Tomate, jamón, muzarella, huevos fritos y aceitunas",
                            "image" => "",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3003",
                            "id" => "29",
                            "name" => "Milanesa ibérica con guarnición",
                            "watson_id" => "Milanesa ibérica con guarnición",
                            "description" => "Jamón crudo, colchón de rúcula, olivas y lascas de parmesano.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/41a02331-41d2-4bb2-aecd-cac7272bb13b.jpg",
                            "price" => "478",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3004",
                            "id" => "30",
                            "name" => "Milanesa yankee con guarnición",
                            "watson_id" => "Milanesa yankee con guarnición",
                            "description" => "Queso cheddar y panceta bacon.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/894a5b35-12f6-463e-adde-d9e00ea67c67.jpg",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3005",
                            "id" => "30",
                            "name" => "Milanesa americana con guarnición",
                            "watson_id" => "Milanesa americana con guarnición",
                            "description" => "Salsa barbacoa, muzzarella, panceta bacon y tomate.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/97b13733-6300-4801-97c9-09c1bd011848.jpg",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3006",
                            "id" => "30",
                            "name" => "Milanesa Mex con guarnición",
                            "watson_id" => "Milanesa Mex con guarnición",
                            "description" => "Salsa de tomate picante y muzzarella.",
                            "image" => "",
                            "price" => "378",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3007",
                            "id" => "30",
                            "name" => "Milanesa Tana con guarnición",
                            "watson_id" => "Milanesa Tana con guarnición",
                            "description" => "Tomate, Muzzarella y aceite de albahaca.",
                            "image" => "",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3008",
                            "id" => "30",
                            "name" => "Milanesa Suiza con guarnición",
                            "watson_id" => "Milanesa Suiza con guarnición",
                            "description" => "Queso muzzarella, queso roquefort y queso parmesano",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/f03dab1a-fc8d-4dd7-81c0-9c73b73e0f16.jpg",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3009",
                            "id" => "30",
                            "name" => "Milanesa Egg con guarnición",
                            "watson_id" => "Milanesa Egg con guarnición",
                            "description" => "Con 2 huevos fritos.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/00b1be29-40db-4c1f-9993-3548da4b7075_d2.jpg",
                            "price" => "378",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3010",
                            "id" => "30",
                            "name" => "Milanesa Food & Love con guarnición",
                            "watson_id" => "Milanesa Food & Love con guarnición",
                            "description" => "Salsa en base a cheddar y bacon.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/051c8993-6bdf-49b8-a48f-fe8109e8e190.jpg",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3011",
                            "id" => "30",
                            "name" => "Milanesa champi con guarnición",
                            "watson_id" => "Milanesa champi con guarnición",
                            "description" => "Milanesa con salsa champignon.",
                            "image" => "",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3012",
                            "id" => "30",
                            "name" => "Milanesa Yankeepower con guarnición",
                            "watson_id" => "Milanesa Yankeepower con guarnición",
                            "description" => "2 huevos fritos, queso cheddar y panceta bacon.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/986d74bb-5149-4c43-86cc-302ea4e10f6a_d2.jpg",
                            "price" => "467",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3013",
                            "id" => "30",
                            "name" => "Milanesa napolitana con guarnición",
                            "watson_id" => "Milanesa napolitana con guarnición",
                            "description" => "Muzzarella, jamón y salsa de tomate.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/b96d69ba-7004-453b-ac53-c8f4f4769017.jpg",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3014",
                            "id" => "30",
                            "name" => "Milanesa fugazzeta con guarnición",
                            "watson_id" => "Milanesa fugazzeta con guarnición",
                            "description" => "Cebolla caramelizada, cebolla morada, orégano, aceite de oliva y muzzarella.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/949b2156-9231-429d-aa38-bdb2248ea793.jpg",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3015",
                            "id" => "30",
                            "name" => "Milanesa texana",
                            "watson_id" => "Milanesa texana",
                            "description" => "Muzzarella gratinada, cebolla y panceta bacon.",
                            "image" => "",
                            "price" => "423",
                            "options_group" => [4, 5],
                        ],
                        [
                            "sku" => "3016",
                            "id" => "30",
                            "name" => "Milanesa sola",
                            "watson_id" => "Milanesa sola",
                            "description" => "",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/29e4b734-6330-4aa9-b95e-c01781ac8078.jpg",
                            "price" => "286",
                            "options_group" => [4, 5],
                        ],
                    ]
                ],
                "4" => [
                    "id" => "4",
                    "name" => "CHIVITOS",
                    "items" => [
                        [
                            "sku" => "4001",
                            "id" => "31",
                            "name" => "Chivito común",
                            "watson_id" => "Chivito común",
                            "description" => "Pan batta, carne, tomate, lechuga, mayonesa, muzarella y jamón. Sin guarnición.",
                            "image" => "",
                            "price" => "400",
                            "options_group" => [2],
                        ],
                        [
                            "sku" => "4002",
                            "id" => "32",
                            "name" => "Chivito Food & Love",
                            "watson_id" => "Chivito Food & Love",
                            "description" => "Pan bata, carne, tomate, huevo frito, queso cheddar, panceta bacon, cebolla y salsa barbacoa.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/7768d48d-8087-45be-9a94-7c4bf24ac53b.jpg",
                            "price" => "400",
                            "options_group" => [2],
                        ],
                        [
                            "sku" => "4003",
                            "id" => "33",
                            "name" => "Chivito canadiense",
                            "watson_id" => "Chivito canadiense",
                            "description" => "",
                            "image" => "",
                            "price" => "400",
                            "options_group" => [2],
                        ],

                    ]
                ],
                "5" => [
                    "id" => "5",
                    "name" => "ENSALADAS",
                    "items" => [
                        [
                            "sku" => "5001",
                            "id" => "34",
                            "name" => "Ensalada César pollo",
                            "watson_id" => "Ensalada César pollo",
                            "description" => "Lechuga crespa, parmesano, pollo, aceitunas y salsa dijon.",
                            "image" => "",
                            "price" => "320",
                        ],
                        [
                            "sku" => "5002",
                            "id" => "35",
                            "name" => "Food and Love salad",
                            "watson_id" => "Food and Love salad",
                            "description" => "Rúcula, jamón crudo, parmesano, lechuga y tomates secos.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/36af94a6-c2b5-4b3e-bd39-85a08c6059d8_d2.jpg",
                            "price" => "320",
                        ],
                        [
                            "sku" => "5003",
                            "id" => "36",
                            "name" => "Ensalada thai",
                            "watson_id" => "Ensalada thai",
                            "description" => "Pollo macerado en soja, jengibre y sésamo con mix de verdes, lascas de parmesano, aderezado con aceto y miel.",
                            "image" => "https://d2vwsr3mua7yp8.cloudfront.net/36ff7573-af34-4f76-a395-a10a01b5b6b6.jpg",
                            "price" => "320",
                        ],
                    ]
                ],
                "6" => [
                    "id" => "6",
                    "name" => "POSTRES",
                    "items" => [
                        [
                            "sku" => "6001",
                            "id" => "37",
                            "name" => "Salchichón de chocolate y dulce de leche (porción)",
                            "watson_id" => "Salchichón de chocolate y dulce de leche",
                            "description" => "",
                            "image" => "",
                            "price" => "125",
                        ],
                    ]
                ],
                "7" => [
                    "id" => "7",
                    "name" => "BEBIDAS",
                    "items" => [
                        [
                            "sku" => "7001",
                            "id" => "38",
                            "name" => "Refresco 500 ml",
                            "watson_id" => "",
                            "description" => "",
                            "image" => "",
                            "price" => "90",
                        ],
                        [
                            "sku" => "7002",
                            "id" => "38",
                            "name" => "Refresco 1.5 lt",
                            "watson_id" => "",
                            "description" => "",
                            "image" => "",
                            "price" => "200",
                        ],
                        [
                            "sku" => "7003",
                            "id" => "38",
                            "name" => "Agua Mineral Salus",
                            "watson_id" => "",
                            "description" => "Con gas",
                            "image" => "",
                            "price" => "90",
                        ],
                    ]
                ],
            ],
            "options_group" => [
                [
                    "sku" => 1,
                    "id" => "guarnicion_picada",
                    "name" => "Guarniciòn",
                    "required" => true,
                    "default" => 8002,
                    "options" => [8001, 8002]
                ],
                [
                    "sku" => 2,
                    "id" => "guarnicion_hamburgesa",
                    "name" => "Guarniciòn",
                    "required" => true,
                    "default" => 8002,
                    "options" => [8001, 8002, 8005]
                ],
                [
                    "sku" => 3,
                    "id" => "burger",
                    "name" => "Burger",
                    "required" => true,
                    "default" => 8003,
                    "options" => [8003, 8004]
                ],
                [
                    "sku" => 4,
                    "id" => "tipo carne",
                    "name" => "Tipo de carne",
                    "required" => true,
                    "default" => 8007,
                    "options" => [8007, 8008, 8009]
                ],
                [
                    "sku" => 5,
                    "id" => "guarnicion_milanesa",
                    "name" => "Guarniciòn",
                    "required" => true,
                    "default" => 8002,
                    "options" => [8002, 8001, 8005, 8006]
                ],
                [
                    "sku" => 6,
                    "id" => "tipo_refresco",
                    "name" => "Línea Pepsi",
                    "required" => true,
                    "options" => [8010, 8011, 8012]
                ],
                [
                    "sku" => 7,
                    "id" => "tipo_agua",
                    "name" => "Tipo",
                    "required" => true,
                    "options" => [8013, 8014]
                ],
            ],
            "variants" => [
                [
                    "sku" => "8001",
                    "id" => "1",
                    "name" => "Papas caseras",
                    "price" => "25",
                    "watson_id" => "caseras"
                ],
                [
                    "sku" => "8002",
                    "id" => "2",
                    "name" => "Papas fritas común",
                    "price" => "0",
                    "watson_id" => "fritas"
                ],
                [
                    "sku" => "8003",
                    "id" => "3",
                    "name" => "Milanesa Clásica",
                    "price" => "0",
                    "watson_id" => "clasica"
                ],
                [
                    "sku" => "8004",
                    "id" => "4",
                    "name" => "Juicy Lucy (rellena de queso Cheddar)",
                    "price" => "38",
                    "watson_id" => "cheddar"
                ],
                [
                    "sku" => "8005",
                    "id" => "5",
                    "name" => "Aros de cebolla",
                    "price" => "25",
                    "watson_id" => "aros cebolla"
                ],

                [
                    "sku" => "8006",
                    "id" => "6",
                    "name" => "Ensalada mixta de lechuga y tomate",
                    "price" => "0",
                    "watson_id" => "ensalada mixta"
                ],
                [
                    "sku" => "8007",
                    "id" => "7",
                    "name" => "Carne",
                    "price" => "0",
                    "watson_id" => "carne"
                ],
                [
                    "sku" => "8008",
                    "id" => "8",
                    "name" => "Pollo",
                    "price" => "0",
                    "watson_id" => "pollo"
                ],
                [
                    "sku" => "8009",
                    "id" => "9",
                    "name" => "Soja",
                    "price" => "0",
                    "watson_id" => "soja"
                ],

                [
                    "sku" => "8010",
                    "id" => "10",
                    "name" => "Pepsi",
                    "price" => "0",
                    "watson_id" => "pepsi"
                ],
                [
                    "sku" => "8011",
                    "id" => "11",
                    "name" => "Pepsi Light",
                    "price" => "0",
                    "watson_id" => "pepsi light"
                ],
                [
                    "sku" => "8012",
                    "id" => "12",
                    "name" => "Paso de los Toros",
                    "price" => "0",
                    "watson_id" => "paso toros"
                ],

                [
                    "sku" => "8013",
                    "id" => "13",
                    "name" => "Con gas",
                    "price" => "0",
                    "watson_id" => "con gas"
                ],
                [
                    "sku" => "8014",
                    "id" => "14",
                    "name" => "Sin gas",
                    "price" => "0",
                    "watson_id" => "sin gas"
                ],
            ]
        ];
    }

}