<?php

namespace App\Http\Controllers\Bot\_1_0_1\traits;

use Dialogflow\WebhookClient;
use Google\Cloud\Dialogflow\V2\QueryInput;
use Google\Cloud\Dialogflow\V2\SessionsClient;
use Google\Cloud\Dialogflow\V2\TextInput;
use App\Behavior;
use App\Company;
use App\Http\Controllers\Nlp\Dialogflow;
use App\Http\Controllers\Dialogflow as DialogflowController;
use App\Http\Controllers\WatsonWebhookController;
use App\Http\Controllers\AiFrameworks\Watson;

trait NlpTrait
{

    public $nplEngine = "dialogflow";

    public function setNlpEngine($engine){
        $this->nplEngine = $engine;
    }

    public function runNlp($params)
    {
        $query = $params["data"]["body"];
        $result = $this->send(
            [
                "query" => $query,
                "lang" => (isset($params["lang"]) && $params["lang"]) ? $params["lang"] : "en-US",
                "sessionId" => $this->session->session_id,
                "mode" => $this->session->mode,
            ]
        );

    }

    public function processNlpResult($response)
    {
        $ret = ["messages" => []];
        foreach ($response->getQueryResult()->getFulfillmentMessages() as $res) {
            $type = $res->getMessage();

            if ($type == "text") {
                $total = count($res->getText()->getText());
                for ($i = 0; $i < $total; $i++) {
                    $m = $res->getText()->getText()[$i];
                    if ($m) {
                        $ret["messages"][] = ["type" => $type, "message" => $m];
                    }
                }

            } elseif ($type == "quick_replies") {
                $quickreplies = $res->getQuickReplies();
                $qr = ["title" => $quickreplies->getTitle(), "messages" => []];
                foreach ($quickreplies->getQuickReplies() as $quickreplie) {
                    $qr["messages"][] = $quickreplie;
                }
                if ($qr["messages"]) {
                    $ret["messages"][] = ["type" => $type, "message" => $qr];;
                }
            }
        }

        return $ret;
    }

    public function send($params)
    {
        if ($this->nplEngine == "dialogflow") {
            return $this->sendDialogflow($params);
        }elseif ($this->nplEngine == "watson") {
            return $this->sendWatson($params);
        }
    }

    public function sendDialogflow($params){
        $dialogflow = new DialogflowController();

        if (is_file(base_path() . DIRECTORY_SEPARATOR . "p-ch-8-kqpxip-650a41e2b6e3.json")) {
            $dialogflow_key_path = base_path() . DIRECTORY_SEPARATOR . "p-ch-8-kqpxip-650a41e2b6e3.json";
        }

        $this->processNlpResult(
            $dialogflow->sendMessageDialogflow(
                $dialogflow_key_path,
                $params["query"],
                $params["sessionId"]
            )
        );
    }

    public function sendWatson($params){
        $watson = new Watson($this->watsonApiUrl, $this->watsonApiKey);
        $result = $watson->send($params["query"], $params["sessionId"]);
        return $result;
    }

    public function reciveWatson($params, $this_){
        $dialog = $params["dialog"];
        $watsonWebhookController = new WatsonWebhookController();
        $result = $watsonWebhookController->process($params, $this_);
        return $result;
    }
}
