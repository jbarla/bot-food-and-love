<?php

namespace App\Http\Controllers\Bot\_1_0_1\traits;

use
    App\Session,
    App\SessionData;

trait SessionTrait
{
    public $session;

    public function sessionInit($to, $wanumber)
    {
        $this->getSession($to, $wanumber);
    }

    public function setSession()
    {
        $to = $this->to;
        $wanumber = $this->from;
        $session = new Session();
        $session->channel = "whatsapp";
        $session->company_id = $this->company_name;
        $session->session_id = $wanumber.".".$this->to;
        $session->mode = "bot";
        $sessionSaved = $session->save();
        if ($sessionSaved) {
            return $session;
        }

        return $session;
    }

    //$this->session = $this->getSession($to, $from . "." . $to);

    private function getSession()
    {
        $to = $this->to;
        $wanumber = $this->from;
        $session = Session::where("channel", "whatsapp")->where("session_id", $wanumber.".".$to)->first();
        return $session;
    }

    public function storeMessageOut($result, $sessionId)
    {
        $message = new Message();

        $message->from_id = $sessionId;
        $message->direction = "out";
        $message->content = json_encode($result);
        $message->metadata = null;
        $message->channel = "webchat";
        $message->type = "object";
        $message->message_time = date("Y-m-d h:i:s");

        if($message->content != "{}"){
            $message->save();
        }
    }

    public function set($attr, $value){
        $SessionData = SessionData::where("attr_name", $attr)->where("session_id", $this->session->session_id)->first();

        if(!$SessionData){
            $SessionData = new SessionData();
            $SessionData->attr_name = $attr;
            $SessionData->session_id = $this->session->session_id;
        }

        $SessionData->attr_value = $value;
        $SessionData->save();
        return $SessionData;
    }

    public function get($attr){
        $SessionData = SessionData::where("attr_name", $attr)->where("session_id", $this->session->session_id)->first();

        if($SessionData){
            return $SessionData->attr_value;
        }

        return null;
    }

}