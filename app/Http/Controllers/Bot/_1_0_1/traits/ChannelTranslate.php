<?php

namespace App\Http\Controllers\Bot\_1_0_1\traits;

use App\Http\Controllers\Controller;

class ChannelTranslate extends Controller
{
    public static function translate($channel, $item)
    {
        $result = false;

        $incomingMsg = $item;

        $type = self::getIncomingType($item);

        if ($type == "text") {
            $result = self::getText($channel, $incomingMsg);
        }elseif ($type == "quickreplies") {
            $result = self::getQuickReplies($channel, $incomingMsg);
        }elseif ($type == "card") {
            $result = self::getCard($channel, $incomingMsg);
        }elseif ($type == "image") {
            $result = self::getImage($channel, $incomingMsg);
        }

        return $result;
    }

    public static function getIncomingType($incoming){
        $type = null;

        if(isset($incoming["text"]) && $incoming["text"]){
            $type = "text";
        }elseif(isset($incoming["image"]) && $incoming["image"]){
            $type = "image";
        }elseif(isset($incoming["quickReplies"]) && $incoming["quickReplies"]){
            $type = "quickreplies";
        }elseif(isset($incoming["card"]) && $incoming["card"]){
            $type = "card";
        }

        return $type;
    }

    public static function getCard($channel, $incomingMsg)
    {
        $result = false;

        if ($channel == "webchat") {
            $result = [
                "type" => "image",
                "uri" => $incomingMsg["card"]["imageUri"],
                "title" => $incomingMsg["card"]["title"]
            ];
        } elseif ($channel == "whatsapp") {
            $uri = $incomingMsg["card"]["imageUri"];
            $type = pathinfo($uri, PATHINFO_EXTENSION);
            $data = file_get_contents($uri);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $result = [
                "type" => "image",
                "uri" => $base64,
                "title" => $incomingMsg["card"]["title"]
            ];
        } elseif ($channel == "facebook") {

        }

        return $result;
    }

    public static function getImage($channel, $incomingMsg)
    {
        $result = false;

        if ($channel == "webchat") {
            $result = [
                "type" => "image",
                "uri" => $incomingMsg["image"]["imageUri"]
            ];
        } elseif ($channel == "whatsapp") {
            $uri = $incomingMsg["image"]["imageUri"];
            $type = pathinfo($uri, PATHINFO_EXTENSION);
            $data = file_get_contents($uri);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $result = [
                "type" => "image",
                "uri" => $base64
            ];
        } elseif ($channel == "facebook") {

        }

        return $result;
    }

    public static function getText($channel, $incomingMsg)
    {
        $result = false;

        $incomingMsg = $incomingMsg["text"];

        if ($channel == "webchat") {
            $respuesta = [
                "type" => "text",
                "messages" => []
            ];
            foreach ($incomingMsg["text"] as $text) {
                $respuesta["messages"][] = $text;
            }
            $result = $respuesta;

        } elseif ($channel == "whatsapp") {
            $respuesta = [
                "type" => "text",
                "messages" => []
            ];
            foreach ($incomingMsg["text"] as $text) {
                $respuesta["messages"][] = $text;
            }
            $result = $respuesta;

        } elseif ($channel == "facebook") {

        }

        return $result;
    }

    public static function getQuickReplies($channel, $incomingMsg)
    {
        $result = false;

        if ($channel == "webchat") {
            $respuesta = [
                "type" => "quickreplies",
                "messages" => []
            ];

            $respuesta["title"] = $incomingMsg["quickReplies"]["title"];
            foreach ($incomingMsg["quickReplies"]["quickReplies"] as $qr) {
                $respuesta["messages"][] = $qr;
            }
            $result = $respuesta;
        }elseif ($channel == "whatsapp") {
            $respuesta = [
                "type" => "quickreplies",
                "messages" => []
            ];

            $respuesta["title"] = $incomingMsg["quickReplies"]["title"];
            foreach ($incomingMsg["quickReplies"]["quickReplies"] as $qr) {
                $respuesta["messages"][] = $qr;
            }
            $result = $respuesta;
        }elseif ($channel == "facebook") {

        }

        return $result;
    }

    //public static function sendImage($uri, $title = ""){}

    public static function sendCard($title, $uri)
    {
        $type = pathinfo($uri, PATHINFO_EXTENSION);
        $data = file_get_contents($uri);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $result = [
            "type" => "image",
            "uri" => $base64,
            "title" => $title
        ];

        return $result;
    }

    public static function sendText($text){
        return [
            "type" => "text",
            "messages" => [$text]
        ];
    }

    public static function sendQuickReplies($title, $quickReplies){

        $respuesta = [
            "type" => "quickreplies",
            "title" => $title,
            "messages" => []
        ];

        $hasIndex = false;
        foreach($quickReplies as $key => $value){
            if($key){
                $hasIndex = true;
            }
            break;
        }

        if($hasIndex){
            foreach($quickReplies as $key => $value){
                $respuesta["messages"][] = $key . " - " .  $value;
            }
        }else{
            foreach($quickReplies as $value){
                $respuesta["messages"][] = $value;
            }
        }

        return $respuesta;
    }
}
