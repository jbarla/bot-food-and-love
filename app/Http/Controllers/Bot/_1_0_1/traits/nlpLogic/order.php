<?php

namespace App\Http\Controllers\Bot\_1_0_1\traits\nlpLogic;

trait order
{

    public function nlpOrdenar($request){
        $message = "";
        $intents = $request["intents"];
        $entities = $request["entities"];
        $saludar = false;
        $intents_valid = [];
        $entities_valid = [];

        foreach ($intents as $intent) {
            if ($intent["confidence"] > $this->confidence) {
                if ($intent["intent"] == "saludar") {
                    $saludar = ($intent["confidence"] > $this->confidence) ? true : false;
                }

                $intents_valid[] = $intent;
            }
        }

        foreach ($entities as $entity) {
            if ($entity["confidence"] > $this->confidence) {
                $entities_valid[] = $entity;
            }
        }

        if ($saludar) {
            $this->results["messages"][] = ["type" => "text", "message" => "Bienvenido a Food and love\n"];
        }

        $entitiesQuery = [
            "HORARIOS" => false,
            "ZONA_ENVIO" => false,
            "PICADAS" => false,
            "BURGERS_GOURMET" => [],
            "MILANESAS_GOURMET_XL" => [],
            "SALUDAR" => $saludar,
        ];

        foreach ($entities_valid as $entity){
            if ($entity["entity"] == "MILANESAS_GOURMET_XL") {
                $entitiesQuery["MILANESAS_GOURMET_XL"][] = $entity["value"];
            }
        }

        if ($entitiesQuery["MILANESAS_GOURMET_XL"]) {
            $type = "MILANESAS_GOURMET_XL";
            $uri = "";
            $products = $this->parent->getProductsByCategories("12");
            foreach ($entitiesQuery["MILANESAS_GOURMET_XL"] as $item) {
                if ($item == "milanesa") {
                    $this->parent->set("slot", json_encode([
                        "action"=> "ordenar",
                        "items"=> [
                            "MILANESAS_GOURMET_XL"
                        ],
                    ]));
                    $this->results["messages"][] = ["type" => "text", "message" => "Podemos ofrecerte estas milanesas\n"];
                    foreach ($products as $product) {
                        $message .= $product["productName"] . "\n";
                    }

                    $this->results["messages"][] = ["type" => "text", "message" => $message];
                } else {

                    foreach ($products as $product) {
                        if ($product["productName"] == $item) {
                            $require = $this->getRequiredOptions($entities_valid, $type);
                            if(!$require){

                            }

                            $message .= "*" . $product["productName"] . "*\n\n";
                            $message .= $product["description"] . "\n";
                            $uri = $product["productPreview"];
                            break;
                        }
                    }

                    if($message){
                        $this->results["messages"][] = ["type" => "card", "title" => $message, "uri" => $uri];
                    }
                }
            }

        }
    }

}
