<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WatsonWebhookController extends Controller
{

    public $results = null;
    public $vendureToken = null;
    public $confidence = 0.4;
    public $validItems = ["MILANESAS_GOURMET_XL", "BURGERS_GOURMET", "BEBIDAS"];
    public $validIntentsOrdenarWatson = ["confirm", "metodos_de_pago", "calles", "MILANESAS_GOURMET_XL", "BURGERS_GOURMET", "GUARNICION_MILANESA", "GUARNICION_HAMBURGUESA", "GUARNICION", "PICADAS", "CARNE_MILANESA", "TIPO_HAMBURGUESA"];
    public $undefinedItems = ["milanesa", "burger"];
    public $categoriesBase = [
        ["name" => "MILANESAS_GOURMET_XL", "alias" => "milanesa", "gender" => "as", "plural" => "s", "id" => 14],
        ["name" => "BURGERS_GOURMET", "alias" => "burger", "gender" => "as", "plural" => "s", "id" => 11],
        ["name" => "PICADAS", "alias" => "picada", "gender" => "as", "plural" => "s", "id" => 11],
        ["name" => "BEBIDAS", "alias" => "bebida", "gender" => "as", "plural" => "s", "id" => 12],
        ["name" => "POSTRES", "alias" => "postre", "gender" => "os", "plural" => "s", "id" => 11],
        ["name" => "CHIVITOS", "alias" => "chivito", "gender" => "os", "plural" => "s", "id" => 13],
        ["name" => "ENSALADAS", "alias" => "ensalada", "gender" => "as", "plural" => "s", "id" => 11],
    ];

    public function __construct()
    {
        $this->results["messages"] = [];
    }

    public function getContext($request)
    {
        $dialog = null;

        $intents = [
            "saludar" => false,
            "ordenar" => false,
            "cambiar" => false,
            "cancelar" => false,
            "consulta_horarios" => false,
            "consultar_carrito" => false,
            "despedida" => false,
        ];

        $entities = [];

        $this->context = $this->prepareContext();

        //$maxIntent = $requestIntent["confidence"];
        foreach ($request["intents"] as $requestIntent) {
            if ($requestIntent["confidence"] >= $this->confidence) {
                foreach ($intents as $intentName => $intentValue) {
                    if ($requestIntent["intent"] == $intentName) {
                        $intents[$intentName] = true;
                    }
                }
            }
        }

        foreach ($request["entities"] as $requestEntities) {
            if ($requestEntities["confidence"] > $this->confidence) {
                $entities[] = $requestEntities;
            }
        }

        $intentDetected = false;
        foreach ($intents as $intentName => $intentValue) {
            foreach ($this->context["intents"] as &$contextIntent) {

                if ($intentName == $contextIntent["name"]) {
                    if ($intentValue) {
                        $intentDetected = true;
                    }
                    $contextIntent["status"] = $intentValue;
                } else {
                    if ($contextIntent["name"] == "consulta") {
                        if (strpos($intentName, ".") !== false) {
                            $consulta = explode(".", $intentName)[1];
                        }

                        $intentName;
                    }
                }

            }
        }

        if (($this->getLastIntent() == "saludar" || $this->getLastIntent() == "ordenar") && !$intentDetected) {
            foreach ($entities as $entity) {
                if (in_array($entity["entity"], $this->validIntentsOrdenarWatson)) {
                    $this->setIntentStatus("ordenar", true);
                }
            }
        }

        //Fix con si me mandaba a despedida
        if ($this->getLastIntent() == "already_address") {
            if (isset($request["entities"][array_search("confirm", array_column($request["entities"], 'entity'))])) {
                $confirm = $request["entities"][array_search("confirm", array_column($request["entities"], 'entity'))];
                if (isset($confirm["confidence"])) {
                    $this->setIntentStatus("ordenar", true);
                } elseif (isset($confirm["value"]) && $confirm["value"] == "metodo_pago") {
                    $this->setIntentStatus("ordenar", true);
                }
            }
        }

        return $this->context;
    }

    public function filterIntent($intents)
    {
        $dialog = "";
    }

    public function index(Request $request)
    {
        return $request->all();
    }

    public function process($request, $this_)
    {
        $this->session_id = $request["session_id"];
        $this->phone = explode(".", $request["session_id"])[0];
        $this->session_id = $request["session_id"];
        $this->parent = $this_;
        $this->context = $this->getContext($request);
        $saludar = true;

        if (strtolower($request["query"]) == "reset") {
            $this->resetContext();
            $this->results["messages"][] = ["type" => "text", "message" => "SESSION RESETEADA"];
        } else {
            $dialog = null;

            foreach ($this->context["intents"] as $intent) {
                if ($intent["status"]) {
                    if ($intent["name"] == "cancelar") {
                        $dialog = "cancelar";
                        break;
                    }
                    if ($intent["name"] == "ordenar") {
                        $dialog = "ordenar";
                        break;
                    }
                    if ($intent["name"] == "consultar_carrito") {
                        $dialog = "consultar_carrito";
                        break;
                    }
                    if ($intent["name"] == "saludar") {
                        $dialog = "saludar";
                        break;
                    }
                }
            }

            $lastIntent = $this->getLastIntent();

            if ($lastIntent == "continuar_comprando") {
                $confirm = $request["entities"][array_search("confirm", array_column($request["entities"], 'entity'))];
                if ($confirm) {
                    if ($confirm["value"] == "no") {
                        $this->despedida();
                    } elseif ($confirm["value"] == "si") {
                        $saludar = false;
                        $dialog = "saludar";
                    }
                }
            } else {
                if (!$dialog) {
                    $despedida = $request["intents"][array_search("despedida", array_column($request["intents"], 'intent'))];
                    if ($despedida) {
                        if ($despedida["confidence"] >= $this->confidence) {
                            $dialog = "despedida";
                        }
                    }
                }
            }

            switch ($dialog) {
                case "saludar":
                    $this->saludar($request, $saludar);
                    break;
                case "consulta":
                    $this->consulta($request);
                    break;
                case "ordenar":
                    $this->ordenar($request);
                    break;
                case "despedida":
                    $this->despedida();
                    break;
                case "cancelar":
                    $this->cancelOrden();
                    $this->results["messages"][] = ["type" => "text", "message" => "¿En que más puedo ayudarlo?"];
                    break;
                case "consultar_carrito":
                    $orden = $this->getCart();
                    if ($orden) {
                        $this->results["messages"][] = ["type" => "text", "message" => $orden];
                    } else {
                        $this->results["messages"][] = ["type" => "text", "message" => "Su carrito esta vacío"];
                    }
                    break;
                default;
            }

            if (empty($this->results["messages"])) {
                $this->results["messages"][] = ["type" => "text", "message" => "¿Podria reformular la pregunta?"];
            }
        }

        return $this->results;
    }

    public function getQueryObject($request)
    {
        $intents = $request["intents"];
        $entities = $request["entities"];
        $saludar = false;
        $intents_valid = [];
        $entities_valid = [];

        foreach ($intents as $intent) {
            if ($intent["confidence"] > $this->confidence) {
                if ($intent["intent"] == "saludar") {
                    $saludar = ($intent["confidence"] > $this->confidence) ? true : false;
                }

                $intents_valid[] = $intent;
            }
        }

        foreach ($entities as $entity) {
            if ($entity["confidence"] > $this->confidence) {
                $entities_valid[] = $entity;
            }
        }


        return [
            "entities" => $entities_valid,
            "intents" => $intents_valid,
            "saludar" => $saludar
        ];
    }

    public function getQuantityEntities($queryObj)
    {
        $result = [];
        foreach ($queryObj["entities"] as $entity) {
            if ($entity["entity"] == "cantidad") {
                $result[] = $entity["value"];
            }
        }

        return $result;
    }

    public function getItemsAndQuantity($queryObj)
    {
        $result = [];
        $quantities = $this->getQuantityEntities($queryObj);
        $pos = 0;
        foreach ($queryObj["entities"] as $entity) {
            if (in_array($entity["entity"], $this->validItems)) {
                $result[] = [
                    "item" => $entity["value"],
                    "quantity" => (isset($quantities[$pos]) && $quantities[$pos]) ? $quantities[$pos] : 1,
                    "category" => $entity["entity"]

                ];
                $pos++;
            }
        }

        $tmp = [];

        foreach($this->undefinedItems as $undefined){
            $pos = 0;
            foreach($result as $r){
                //filtro burguer indefinidas si tiene tambien una burguer de un tipo especifico ejemplo seetlove o otro tipo
                if($r["item"] == $undefined){
                    $tipoDefinido = false;
                    foreach($result as $tmpR){
                        if($tmpR["category"] == "BURGERS_GOURMET"){
                            if($tmpR["item"] != $undefined){
                                $tipoDefinido = true;
                                break;
                            }
                        }
                    }
                    if($tipoDefinido){
                        unset($result[$pos]);
                    }
                }
                $pos++;
            }
        }

        return $result;
    }

    public function getAddress()
    {
        $user_data = json_decode($this->parent->get("user_data." . $this->session_id), true);
        if (isset($user_data["address"]) && $user_data["address"]) {
            return $user_data["address"];
        }

        return false;
    }

    public function getUserData()
    {
        return json_decode($this->parent->get("user_data." . $this->session_id), true);
    }

    public function setUserData($data, $value)
    {
        $userData = $this->getUserData();
        $userData[$data] = $value;
        $this->parent->set("user_data." . $this->session_id, json_encode($userData));
    }

    public function resetUserData()
    {
        $this->parent->set("user_data." . $this->session_id, json_encode([]));
    }

    public function getPaymentMethod()
    {
        return null;
    }

    public function getIntentAttr($intent, $nameAttr, $default = false)
    {
        $attr = $default;
        if (isset($this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))])) {
            if (isset($this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))][$nameAttr])) {
                $attr = $this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))][$nameAttr];
                if ($attr === null) {
                    $attr = $default;
                }
            }
        }

        return $attr;
    }

    public function getIntentStatus($intent)
    {
        $status = false;
        if (isset($this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))])) {
            if (isset($this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))]["status"])) {
                $status = $this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))]["status"];
                if ($status === null) {
                    $status = false;
                }
            }
        }

        return $status;
    }

    public function setIntentStatus($intent, $status)
    {
        if (isset($this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))])) {
            $this->context["intents"][array_search($intent, array_column($this->context["intents"], 'name'))]["status"] = $status;
            $this->setContext($this->context);
        }
    }

    public function setIntentContext($intentName, $intentArray)
    {
        $this->context["intents"][array_search($intentName, array_column($this->context["intents"], 'name'))] = $intentArray;
        $this->parent->set("context." . $this->session_id, json_encode($this->context));
    }

    public function setItemContext($intentName, $item)
    {
        $this->context["intents"][array_search($intentName, array_column($this->context["intents"], 'name'))]["items"][] = $item;
        $this->parent->set("context." . $this->session_id, json_encode($this->context));
    }

    public function updateItemsContext($intentName, $items)
    {
        $this->context["intents"][array_search($intentName, array_column($this->context["intents"], 'name'))]["items"] = $items;
        $this->parent->set("context." . $this->session_id, json_encode($this->context));
    }

    public function updateOrdenar($intentName, $status)
    {
        $this->context["intents"][array_search($intentName, array_column($this->context["intents"], 'name'))] = $status;
        $this->parent->set("context." . $this->session_id, json_encode($this->context));
    }

    public function getIntentContext($intent)
    {
        return $this->context["intents"] [array_search($intent, array_column($this->context["intents"], 'name'))];
    }

    public function isUndefinedItem($item)
    {

        $return = [
            "success" => true,
            "category" => "",
        ];

        $pos = array_search($item, $this->undefinedItems);

        if ($pos !== false) {
            $return["success"] = false;
            $cat = $this->categoriesBase[array_search($this->undefinedItems[$pos], array_column($this->categoriesBase, 'alias'))];
            $return["category"] = $cat;
        }

        return $return;
    }

    public function getListCategory($category)
    {
        $message = "Estas son nuestr" . $category["gender"] . " " . $category["alias"] . $category["plural"] . "\n\n";

        $products = $this->parent->getProductsByCategories($category["id"], $this->session_id);
        foreach ($products as $product) {
            $message .= $product["productName"] . "\n";
        }

        return $message;
    }

    public function mergeRequestsEntities($lastRequest, $request)
    {
        $merge = [];
        $exist = [];
        $pos = [];

        foreach ($lastRequest["entities"] as $oldEntity) {
            if (!in_array($oldEntity["entity"], $exist)) {
                $exist[] = $oldEntity["entity"];
                $merge[] = $oldEntity;
            } elseif ($oldEntity["entity"] == "cantidad") {
                $merge[] = $oldEntity;
            }
        }

        foreach ($request["entities"] as $currentEntity) {
            if (!in_array($currentEntity["entity"], $exist)) {
                $exist[] = $currentEntity["entity"];
                $merge[] = $currentEntity;
            } elseif ($currentEntity["entity"] == "cantidad") {
                $merge[] = $currentEntity;
            } elseif ($currentEntity["entity"] == "MILANESAS_GOURMET_XL") {
                $pos = 0;
                foreach ($merge as $m) {
                    if ($m["entity"] == "MILANESAS_GOURMET_XL") {
                        if ($this->isUndefinedItem($m["value"])) {
                            unset($merge[$pos]);
                            array_splice($merge, $pos, 0, [$currentEntity]);

                        }
                    }
                    $pos++;
                }
            } elseif ($currentEntity["entity"] == "BURGERS_GOURMET") {
                $pos = 0;
                foreach ($merge as $m) {
                    if ($m["entity"] == "BURGERS_GOURMET") {
                        if ($this->isUndefinedItem($m["value"])) {
                            unset($merge[$pos]);
                            array_splice($merge, $pos, 0, [$currentEntity]);
                        }
                    }
                    $pos++;
                }
            }
        }

        return $merge;
    }

    public function searchItem($entity, $intentOrdenar)
    {
        foreach ($intentOrdenar["items"] as $item) {
            if ($item["quantity"] == $entity["quantity"] && $item["id"] == $entity["id"]) {
                return true;
            }
        }

        return false;
    }

    public function setIntentOrdenar($items, $itemsBase, $queryObj, $intentOrdenarItems)
    {
        $intentOrdenar["items"] = $intentOrdenarItems;
        foreach ($items as $item) {

            foreach ($itemsBase as $key => $entity) {
                if ($entity["required"] == true) {
                    if ($key == $item["category"]) {
                        $i = $this->isUndefinedItem($item["item"]);
                        if (!$i["success"]) {
                            $intentOrdenar["status"] = "pending";
                            $message = $this->getListCategory($i["category"]);
                            $this->setIntentContext("ordenar", $intentOrdenar);
                            $consultaPorItem = true;
                            $this->results["messages"][] = ["type" => "text", "message" => $message];
                            break;
                        } else {
                            $watonsProduct = $this->getWatsonProduct($item["item"]);
                            if ($watonsProduct) {
                                $product = $this->parent->getProduct($watonsProduct["id"], $this->session_id);
                                $entity["id"] = $watonsProduct["id"];
                                $entity["name"] = $item["item"];
                                $entity["quantity"] = $item["quantity"];
                                //if($lastRequest){
                                //if(!$this->searchItem($entity, $intentOrdenar)){
                                $intentOrdenar["items"][] = $entity;
                                //}
                                //}
                                break;
                            }
                        }
                    }
                }
            }
        }

        if ($intentOrdenar["items"]) {
            $pos = 0;
            $optionAdded = false;
            foreach ($intentOrdenar["items"] as $item) {
                foreach ($queryObj["entities"] as $entitiy) {
                    foreach ($item as $key => $value) {
                        if ($entitiy["entity"] == $key) {
                            if ("GUARNICION_MILANESA" == $entitiy["entity"]) {
                                $guarnicionCode = $this->getWatsonGuarniciones($entitiy["value"]);
                                $intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $guarnicionCode["code"]];
                                $optionAdded = true;
                            } elseif ("CARNE_MILANESA" == $entitiy["entity"]) {
                                //$tipoCarne = $this->getWatsonAttr($entitiy["value"]);
                                //$intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $guarnicionCode];
                                $intentOrdenar["items"][$pos][$key] = $entitiy["value"];
                                $optionAdded = true;
                            } elseif ("GUARNICION_HAMBURGUESA" == $entitiy["entity"]) {
                                $guarnicionCode = $this->getWatsonGuarniciones($entitiy["value"]);
                                $intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $guarnicionCode["code"]];
                                //$intentOrdenar["items"][$pos][$key] = $entitiy["value"];
                                $optionAdded = true;
                            } elseif ("TIPO_HAMBURGUESA" == $entitiy["entity"]) {
                                $tipoHamburguesa = $this->getWatsonTiposCarne($entitiy["value"]);
                                $intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $tipoHamburguesa["code"]];
                                $optionAdded = true;
                            }

                        }
                    }
                }
                $pos++;
            }

            if ($optionAdded) {
                $this->updateItemsContext("ordenar", $intentOrdenar["items"]);
            }
        }

        foreach ($itemsBase as $key => $entity) {
            foreach ($queryObj["entities"] as $entityQuery) {
                //if($key == $entityQuery["entity"]){
                foreach ($entity as $k => $v) {
                    if ($k == $entityQuery["entity"]) {
                        $itemsBase[$key][$k] = $entityQuery["value"];
                    }
                }
                //}
            }
        }

        return $intentOrdenar;
    }

    public function filterGuarnicionesCruzadas($queryObj)
    {
        /*
         * Se puede complicar mas porque ademas se cruza con una entidad que es picada (ver despues)
         */
        $hamburguesa = false;
        $milanesa = false;

        if (array_search("BURGERS_GOURMET", array_column($queryObj["entities"], 'entity')) !== false) {
            $hamburguesa = $queryObj["entities"][array_search("BURGERS_GOURMET", array_column($queryObj["entities"], 'entity'))];
        }

        if (array_search("MILANESAS_GOURMET_XL", array_column($queryObj["entities"], 'entity')) !== false) {
            $milanesa = $queryObj["entities"][array_search("MILANESAS_GOURMET_XL", array_column($queryObj["entities"], 'entity'))];
        }


        $guarnicionHamburguesa = false;
        $guarnicionMilanesa = false;

        if (array_search("GUARNICION_HAMBURGUESA", array_column($queryObj["entities"], 'entity')) !== false) {
            $guarnicionHamburguesa = $queryObj["entities"][array_search("GUARNICION_HAMBURGUESA", array_column($queryObj["entities"], 'entity'))];
        }

        if (array_search("GUARNICION_HAMBURGUESA", array_column($queryObj["entities"], 'entity')) !== false) {
            $guarnicionMilanesa = $queryObj["entities"][array_search("GUARNICION_MILANESA", array_column($queryObj["entities"], 'entity'))];
        }

        //Si estan las 2 entities (hamburguesa y milanesa)
        if ($hamburguesa && $milanesa) {
            //Primero me fijo si vienen las 2 guarniciones
            if ($guarnicionHamburguesa && $guarnicionMilanesa) {
                //Me fijo si las 2 entidades estan repetidas en su value
                if ($guarnicionHamburguesa["value"] == $guarnicionMilanesa["value"]) {
                    //Obtengo la posicion de las entidades para saber cual viene antes
                    $posHamburguesa = array_search("BURGERS_GOURMET", array_column($queryObj["entities"], 'entity'));
                    $posMilanesa = array_search("MILANESAS_GOURMET_XL", array_column($queryObj["entities"], 'entity'));

                    //Si la hamburguesa esta antes elimino la guarnicion milanesa
                    if ($posHamburguesa < $posMilanesa) {
                        unset($queryObj["entities"][array_search("GUARNICION_MILANESA", array_column($queryObj["entities"], 'entity'))]);
                    } else {
                        //sino elimino la guarnicion hamburguesa
                        unset($queryObj["entities"][array_search("GUARNICION_HAMBURGUESA", array_column($queryObj["entities"], 'entity'))]);
                    }
                }
                //me fijo que entidad esta primero
            }
        } else {
            //Si solo ingresa la guarnicion
            if (!$hamburguesa && !$milanesa) {
                $intentOrdenar = $this->getIntentContext("ordenar");
                if ($guarnicionHamburguesa && $guarnicionMilanesa) {
                    foreach ($intentOrdenar["items"] as $item) {
                        if (isset($item["GUARNICION_HAMBURGUESA"]) && $item["GUARNICION_HAMBURGUESA"]) {
                            unset($queryObj["entities"][array_search("GUARNICION_HAMBURGUESA", array_column($queryObj["entities"], 'entity'))]);
                            break;
                        } elseif (isset($item["GUARNICION_MILANESA"]) && $item["GUARNICION_MILANESA"]) {
                            unset($queryObj["entities"][array_search("GUARNICION_MILANESA", array_column($queryObj["entities"], 'entity'))]);
                            break;
                        }
                    }
                }
            }
        }

        return $queryObj;

    }

    public function ordenar($request)
    {
        $message = "";

        //fix clasica
        $request["query"] = str_replace("clásica", "clasica", strtolower(trim($request["query"])));
        if(trim(strtolower($request["query"])) == "clasica" || trim(strtolower($request["query"])) == "la clasica" || trim(strtolower($request["query"])) == "una clasica" || strpos(trim(strtolower($request["query"])), "una clasica") !== false){
            $tmp = [];
            foreach($request["entities"] as $entity){
                if($entity["entity"] == "TIPO_HAMBURGUESA"){
                    $tmp[] = $entity;
                }
            }
            $request["entities"] = $tmp;
        }

        $this->confirm = "";
        $lastIntent = $this->getLastIntent();
        $lastRequest = $this->getLastRequest();

        if ($lastRequest) {
            $request["entities"] = $this->mergeRequestsEntities($lastRequest, $request);
        } else {
            $request = $this->filterGuarnicionesCruzadas($request);
        }

        if ($lastIntent != "already_address") {
            $this->setLastIntent("ordenar");
        }

        $queryObj = $this->getQueryObject($request);

        $items = $this->getItemsAndQuantity($queryObj);
        $address = "";//$this->getAddress();

        //$payment_method = $this->getPaymentMethod();
        $itemsBase = $this->entitySlotsBase();

        $intentOrdenar = $this->getIntentContext("ordenar");

        if ($this->getIntentStatus("saludar") && !$intentOrdenar["items"]) {
            $this->results["messages"][] = ["type" => "text", "message" => "Bienvenido a Food and love\n"];
        }

        $requires = [
            "address" => isset($intentOrdenar["address"]) ? $intentOrdenar["address"] : false,
            "payment_method" => isset($intentOrdenar["payment_method"]) ? $intentOrdenar["payment_method"] : false,
            "confirm" => isset($intentOrdenar["confirm"]) ? $intentOrdenar["confirm"] : false,
        ];

        if ($intentOrdenar) {
            foreach ($queryObj["entities"] as $entitiy) {
                if ($entitiy["entity"] == "calles") {
                    $requires["address"] = $request["query"];
                    $intentOrdenar["address"] = $request["query"];
                    $this->setUserData("address", $request["query"]);
                    $this->setIntentContext("ordenar", $intentOrdenar);
                }
                if ($entitiy["entity"] == "metodos_de_pago") {
                    $requires["payment_method"] = $entitiy["value"];

                    $intentOrdenar["payment_method"] = $entitiy["value"];
                    $this->setIntentContext("ordenar", $intentOrdenar);
                }
                if ($entitiy["entity"] == "confirm") {
                    $this->confirm = $entitiy["value"];
                    if ($intentOrdenar["items"] && $requires["payment_method"] && $requires["address"]) {
                        $requires["confirm"] = $entitiy["value"];
                        $this->setIntentContext("ordenar", $intentOrdenar);
                    }
                }
            }
        }

        //Setear item ( milanesa, hamburguesa, etc
        $consultaPorItem = false;
        foreach ($items as $item) {

            foreach ($itemsBase as $key => $entity) {
                if ($entity["required"] == true) {
                    if ($key == $item["category"]) {
                        $i = $this->isUndefinedItem($item["item"]);
                        if (!$i["success"]) {
                            $intentOrdenar["status"] = "pending";
                            $message = $this->getListCategory($i["category"]);
                            $this->setIntentContext("ordenar", $intentOrdenar);
                            $consultaPorItem = true;
                            $this->results["messages"][] = ["type" => "text", "message" => $message];
                            break;
                        } else {
                            $watonsProduct = $this->getWatsonProduct($item["item"]);
                            if ($watonsProduct) {
                                $product = $this->parent->getProduct($watonsProduct["id"], $this->session_id);
                                $entity["id"] = $watonsProduct["id"];
                                $entity["name"] = $item["item"];
                                $entity["quantity"] = $item["quantity"];
                                //if($lastRequest){
                                //if(!$this->searchItem($entity, $intentOrdenar)){
                                $this->setItemContext("ordenar", $entity);
                                $intentOrdenar["items"][] = $entity;
                                //}
                                //}
                                break;
                            }
                        }
                    }
                }
            }
        }

        $intentOrdenar = $this->getIntentContext("ordenar");

        if ($intentOrdenar["items"]) {
            $pos = 0;
            $optionAdded = false;
            foreach ($intentOrdenar["items"] as $item) {
                foreach ($queryObj["entities"] as $entitiy) {
                    foreach ($item as $key => $value) {
                        if ($entitiy["entity"] == $key) {
                            if ("GUARNICION_MILANESA" == $entitiy["entity"]) {
                                $guarnicionCode = $this->getWatsonGuarniciones($entitiy["value"]);
                                $intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $guarnicionCode["code"]];
                                $optionAdded = true;
                            } elseif ("CARNE_MILANESA" == $entitiy["entity"]) {
                                //$tipoCarne = $this->getWatsonAttr($entitiy["value"]);
                                //$intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $guarnicionCode];
                                $intentOrdenar["items"][$pos][$key] = $entitiy["value"];
                                $optionAdded = true;
                            } elseif ("GUARNICION_HAMBURGUESA" == $entitiy["entity"]) {
                                $guarnicionCode = $this->getWatsonGuarniciones($entitiy["value"]);
                                $intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $guarnicionCode["code"]];
                                //$intentOrdenar["items"][$pos][$key] = $entitiy["value"];
                                $optionAdded = true;
                            } elseif ("TIPO_HAMBURGUESA" == $entitiy["entity"]) {
                                $tipoHamburguesa = $this->getWatsonTiposCarne($entitiy["value"]);
                                $intentOrdenar["items"][$pos][$key] = ["qty" => 1, "code" => $tipoHamburguesa["code"]];
                                $optionAdded = true;
                            }

                        }
                    }
                }
                $pos++;
            }

            if ($optionAdded) {
                $this->updateItemsContext("ordenar", $intentOrdenar["items"]);
            }
        }

        foreach ($itemsBase as $key => $entity) {
            foreach ($queryObj["entities"] as $entityQuery) {
                //if($key == $entityQuery["entity"]){
                foreach ($entity as $k => $v) {
                    if ($k == $entityQuery["entity"]) {
                        $itemsBase[$key][$k] = $entityQuery["value"];
                    }
                }
                //}
            }
        }

        if ($consultaPorItem) {
            $this->setLastRequest($request);
            $this->setTmpItems($intentOrdenar["items"]);
            $intentOrdenar["items"] = [];
            $this->updateItemsContext("ordenar", $intentOrdenar["items"]);
            return;
        } else {
            $this->setTmpItems([]);
            $this->setLastRequest([]);
        }

        //Si tengo item y no tiene guarnicion
        foreach ($itemsBase as $entity) {
            foreach ($queryObj["entities"] as $queryEntity) {
                if ($queryEntity["entity"] == "GUARNICION_MILANESA") {
                    if (in_array("GUARNICION_MILANESA", $queryObj["entities"])) {
                        if (isset($entity["GUARNICION_MILANESA"]) && !$entity["GUARNICION_MILANESA"]) {
                            $this->results["messages"][] = ["type" => "text", "message" => "Con que guarnición desea su milanesa:\n"];

                            $watonsProduct = $this->getWatsonProduct($item["item"]);
                            if ($watonsProduct) {
                                $product = $this->parent->getProduct($watonsProduct["id"], $this->session_id);
                                $guarnicionesDisponibles = $this->getProductGuarniciones($product);
                                $message = "";
                                foreach ($guarnicionesDisponibles as $g) {
                                    $message .= $g . "\n";
                                }

                                $entity["id"] = $watonsProduct["id"];
                                $entity["name"] = $item["item"];
                                $this->setItemContext("ordenar", $entity);

                                return $this->results["messages"][] = ["type" => "text", "message" => $message . "\n"];
                            }
                        }
                    }
                } elseif ($queryEntity["entity"] == "GUARNICION_HAMBURGUESA") {
                    if (in_array("GUARNICION_HAMBURGUESA", $queryObj["entities"])) {
                        if (isset($entity["GUARNICION_HAMBURGUESA"]) && !$entity["GUARNICION_HAMBURGUESA"]) {
                            $this->results["messages"][] = ["type" => "text", "message" => "Con que guarnición desea su Burger:\n"];

                            $watonsProduct = $this->getWatsonProduct($item["item"]);
                            $product = $this->parent->getProduct($watonsProduct["id"], $this->session_id);
                            $guarnicionesDisponibles = $this->getProductGuarniciones($product);
                            $message = "";
                            foreach ($guarnicionesDisponibles as $g) {
                                $message .= $g . "\n";
                            }

                            $entity["id"] = $watonsProduct["id"];
                            $entity["name"] = $item["item"];
                            $this->setItemContext("ordenar", $entity);

                            return $this->results["messages"][] = ["type" => "text", "message" => $message . "\n"];
                        }
                    }
                }
            }
        }

        //Preguntar
        //
        foreach ($intentOrdenar["items"] as $item) {
            if (isset($item["GUARNICION_MILANESA"]) && !$item["GUARNICION_MILANESA"]) {
                return $this->results["messages"][] = ["type" => "text", "message" => "Con que guarnición desea su milanesa:\n"];
            }
            if (isset($item["CARNE_MILANESA"]) && !$item["CARNE_MILANESA"]) {
                return $this->results["messages"][] = ["type" => "text", "message" => "Milanesa de carne, pollo o soja?\n"];
            }
            if (isset($item["TIPO_HAMBURGUESA"]) && !$item["TIPO_HAMBURGUESA"]) {
                return $this->results["messages"][] = ["type" => "text", "message" => "Hamburguesa clasica o  Juicy Lucy (rellena de queso cheddar) ?\n"];
            }
            if (isset($item["GUARNICION_HAMBURGUESA"]) && !$item["GUARNICION_HAMBURGUESA"]) {
                //} if ((isset($item["GUARNICION_HAMBURGUESA"]) && !$item["GUARNICION_HAMBURGUESA"]) || ( !isset($item["GUARNICION_HAMBURGUESA"]["code"]) || !$item["GUARNICION_HAMBURGUESA"]["code"])) {
                $this->results["messages"][] = ["type" => "text", "message" => "Con que guarnición desea su Burger?\n"];

                $watonsProduct = $this->getWatsonProduct($item["name"]);
                $product = $this->parent->getProduct($watonsProduct["id"], $this->session_id);
                $guarnicionesDisponibles = $this->getProductGuarniciones($product);
                $message = "";
                foreach ($guarnicionesDisponibles as $g) {
                    $message .= $g . "\n";
                }

                $entity["id"] = $watonsProduct["id"];
                $entity["name"] = $item["name"];
                $this->setItemContext("ordenar", $entity);

                return $this->results["messages"][] = ["type" => "text", "message" => $message . "\n"];
            }
        }

        $lastIntent = $this->getLastIntent();

        //if ($lastIntent != "already_address") {
        $address = $this->getAddress();

        if ($lastIntent == "already_address") {
            if ($this->confirm == "no" && (!isset($queryObj["entities"][array_search("calles", array_column($queryObj["entities"], 'entity'))]))) {
                    $requires["address"] = false;
            } elseif ($this->confirm == "si") {
                $requires["address"] = $address;
                $intentOrdenar["address"] = $address;
                $this->setIntentContext("ordenar", $intentOrdenar);
            }else{
                if(isset($queryObj["entities"][array_search("calles", array_column($queryObj["entities"], 'entity'))])){
                    $newAddress = $queryObj["entities"][array_search("calles", array_column($queryObj["entities"], 'entity'))];
                    $requires["address"] = $newAddress["value"] . " " .substr($request["query"], $newAddress["location"][1], strlen($request["query"]));
                    $this->setUserData("address", $requires["address"]);
                    $intentOrdenar["address"] = $requires["address"];
                    $this->setIntentContext("ordenar", $intentOrdenar);
                }
            }
            $this->setLastIntent("ordenar");
        } else {
            if ($address && !$requires["address"]) {
                $message = "Se lo enviamos a " . $address . "?\n";
                $message .= "De lo contrario ingrese la nueva direcciòn\n";
                $this->setLastIntent("already_address");
                return $this->results["messages"][] = ["type" => "text", "message" => $message];
            }
        }

        if ($requires["address"] == false) {
            return $this->results["messages"][] = ["type" => "text", "message" => "Cual es su dirección de envío?\n"];
        }

        if ($requires["payment_method"] == false) {
            return $this->results["messages"][] = ["type" => "text", "message" => "Seleccione Metodo de pago\nEfectivo o Tarjeta de credito/Debito\n"];
        }

        if ($requires["confirm"] === false) {
            $orden = $this->getCart();
            $this->results["messages"][] = ["type" => "text", "message" => $orden];
            return $this->results["messages"][] = ["type" => "text", "message" => "Por favor, confirma su orden?\nSi/No\n"];
            //return $this->results["messages"][] = ["type" => "text", "message" => "Por favor, confirma su orden?\nSi/No\n"];
        }

        if ($requires["confirm"] === "si") {
            return $this->confirmOrder();
        } elseif ($requires["confirm"] === "no") {
            return $this->cancelOrden();
        }
    }

    public function saludar($request, $saludar = true)
    {
        $message = "Podemos ofrecerle:\n";
        $message .= "Picadas\n";
        $message .= "Burguers\n";
        $message .= "Milanesas\n";
        $message .= "Ensaladas\n";
        $message .= "Chivitos\n";
        $message .= "Postres\n";
        $message .= "Bebidas\n";

        if ($saludar) {
            $this->results["messages"][] = ["type" => "text", "message" => "Bienvenido a Food and love\n"];
            $this->results["messages"][] = ["type" => "text", "message" => $message];
        }else{
            $this->results["messages"][] = ["type" => "text", "message" => "¿En que otra cosa podemos ayudarlo?\n"];
        }


        $this->setLastIntent("saludar");
    }

    public function setLastIntent($intent)
    {
        $this->context["metadata"]["last_intent"] = $intent;
        $this->parent->set("context." . $this->session_id, json_encode($this->context));
    }

    public function setLastRequest($request)
    {
        $this->context["metadata"]["last_request"] = json_encode($request);
        $this->parent->set("context." . $this->session_id, json_encode($this->context));
    }

    public function setTmpItems($request)
    {
        $this->context["metadata"]["tmp_items"] = json_encode($request);
        $this->parent->set("context." . $this->session_id, json_encode($this->context));
    }


    public function getLastIntent()
    {
        return $this->context["metadata"]["last_intent"];
    }

    public function getLastRequest()
    {
        return json_decode($this->context["metadata"]["last_request"], true);
    }

    public function getTmpItems()
    {
        return json_decode($this->context["metadata"]["tmp_items"], true);
    }

    public function consulta($request)
    {
        $this->setLastIntent("consulta");
        $this->parent->set("slot", null);
        $message = "";
        $intents = $request["intents"];
        $entities = $request["entities"];
        $saludar = false;
        foreach ($intents as $intent) {
            if ($intent["intent"] == "saludar") {
                $saludar = ($intent["confidence"] > $this->confidence) ? true : false;
            }
        }

        $entitiesQuery = [
            "HORARIOS" => false,
            "ZONA_ENVIO" => false,
            "PICADAS" => false,
            "BURGERS_GOURMET" => [],
            "MILANESAS_GOURMET_XL" => [],
            "SALUDAR" => $saludar,
            "SOLO_SALUDAR" => true,
        ];

        foreach ($entities as $entity) {
            if ($entity["entity"] == "BURGERS_GOURMET") {
                $entitiesQuery["BURGERS_GOURMET"][] = $entity["value"];
                $entitiesQuery["SOLO_SALUDAR"] = false;
            }

            if ($entity["entity"] == "MILANESAS_GOURMET_XL") {
                $entitiesQuery["MILANESAS_GOURMET_XL"][] = $entity["value"];
                $entitiesQuery["SOLO_SALUDAR"] = false;
            }

            if ($entity["entity"] == "PICADAS") {
                $entitiesQuery["PICADAS"] = true;
                $entitiesQuery["SOLO_SALUDAR"] = false;
            }
            if ($entity["entity"] == "horarios") {
                $entitiesQuery["HORARIOS"] = true;
                $entitiesQuery["SOLO_SALUDAR"] = false;
            }
            if ($entity["entity"] == "zona_envio") {
                $entitiesQuery["ZONA_ENVIO"] = true;
                $entitiesQuery["SOLO_SALUDAR"] = false;
            }
        }

        if ($saludar) {
            $this->results["messages"][] = ["type" => "text", "message" => "Bienvenido a Food and love\n"];
        }

        if ($entitiesQuery["HORARIOS"]) {
            $message .= "Estamos abiertos de lunes a viernes de 09:00 a 18:00hs\n";
            $message .= "y los fines de semana de 09:00 a 23:00hs\n";
            $this->results["messages"][] = ["type" => "text", "message" => $message];
            $message = "";
        }

        if ($entitiesQuery["ZONA_ENVIO"]) {
            $message .= "Los envios se hacen en la zona de pocitos.\n";
            $message .= "Puede consultar si enviamos donde ud se encuentra ingresando: consultar area de envio.\n";
            $this->results["messages"][] = ["type" => "text", "message" => $message];
            $message = "";
        }

        if ($entitiesQuery["PICADAS"]) {

            $message .= "Estas son nuestras picadas\n\n";
            $message .= "Picada Food & Love $872\n";
            $message .= "Papas champi $312\n";
            $message .= "Papas Food & Love $312\n";
            $message .= "Papas Mex $312\n";
            $message .= "Papas Barbacoa $312\n";
            $message .= "Papas caseras $180\n";
            $message .= "Aros de cebolla $200\n";

            $this->results["messages"][] = ["type" => "text", "message" => $message];
        }

        if ($entitiesQuery["BURGERS_GOURMET"]) {
            $products = $this->parent->getProductsByCategories("11", $this->session_id);
            foreach ($entitiesQuery["BURGERS_GOURMET"] as $item) {
                if ($item == "burger") {

                    foreach ($products as $product) {
                        $message .= $product["productName"] . "\n";
                    }

                    $this->results["messages"][] = ["type" => "text", "message" => $message];
                } else {
                    foreach ($products as $product) {
                        if ($product["productName"] == $item) {
                            $message .= "*" . $product["productName"] . "*\n\n";
                            $message .= $product["description"] . "\n";
                            $uri = $product["productPreview"];
                            break;
                        }
                    }

                    $this->results["messages"][] = ["type" => "card", "title" => $message, "uri" => $uri];
                }
            }

        }

        if ($entitiesQuery["MILANESAS_GOURMET_XL"]) {
            $uri = "";
            $products = $this->parent->getProductsByCategories("12", $this->session_id);
            foreach ($entitiesQuery["MILANESAS_GOURMET_XL"] as $item) {
                if ($item == "milanesa") {

                    $this->results["messages"][] = ["type" => "text", "message" => "Estas son nuestras milanesas\n"];

                    foreach ($products as $product) {
                        $message .= $product["productName"] . "\n";
                    }

                    $this->results["messages"][] = ["type" => "text", "message" => $message];
                } else {

                    foreach ($products as $product) {
                        if ($product["productName"] == $item) {
                            $message .= "*" . $product["productName"] . "*\n\n";
                            $message .= $product["description"] . "\n";
                            $uri = $product["productPreview"];
                            break;
                        }
                    }

                    if ($message) {
                        $this->results["messages"][] = ["type" => "card", "title" => $message, "uri" => $uri];
                    }
                }
            }

        }

        if ($entitiesQuery["SOLO_SALUDAR"] && $entitiesQuery["SALUDAR"]) {
            $message .= "Que desea ordenar?\n";
            foreach ($this->parent->watsonCategories() as $key => $wc) {
                $message .= $wc . "\n";
            }

            $this->results["messages"][] = ["type" => "text", "message" => $message];
        }

    }

    public function resetContext()
    {
        $this->setContext([]);
        $this->resetUserData();
        $this->setLastIntent("");
        $this->vendureToken = "";

        return $this->prepareContext(true);
    }

    public function cancelOrden()
    {
        $intentOrdenar = $this->getIntentContext("ordenar");
        $intentOrdenar["items"] = [];
        $this->updateItemsContext("ordenar", $intentOrdenar["items"]);
        $this->setContext($this->context);
        $this->prepareContext();
        $this->vendureToken = "";
        return $this->results["messages"][] = ["type" => "text", "message" => "Su orden ha sido cancelada\n"];
    }

    public function setContext($context)
    {
        $this->parent->set("context." . $this->session_id, json_encode($context));
        return (array)json_decode($this->parent->get("context." . $this->session_id), true);
    }

    public function prepareContext($restart = false)
    {
        $session_context = (array)json_decode($this->parent->get("context." . $this->session_id), true);
        if ($restart) {
            $session_context = [];
        }
        $session_context["metadata"] = (isset($session_context["metadata"]) && $session_context["metadata"]) ? $session_context["metadata"] : [];
        $session_context["metadata"]["session_id"] = (isset($session_context["metadata"]["session_id"]) && $session_context["metadata"]["session_id"]) ? $session_context["metadata"]["session_id"] : [];
        $session_context["metadata"]["lang"] = (isset($session_context["metadata"]["lang"]) && $session_context["metadata"]["lang"]) ? $session_context["metadata"]["lang"] : [];
        $session_context["metadata"]["date_time"] = (isset($session_context["metadata"]["date_time"]) && $session_context["metadata"]["date_time"]) ? $session_context["metadata"]["date_time"] : [];
        $session_context["metadata"]["timezone"] = (isset($session_context["metadata"]["timezone"]) && $session_context["metadata"]["timezone"]) ? $session_context["metadata"]["timezone"] : [];
        $session_context["metadata"]["last_intent"] = (isset($session_context["metadata"]["last_intent"]) && $session_context["metadata"]["last_intent"]) ? $session_context["metadata"]["last_intent"] : [];
        $session_context["metadata"]["last_request"] = (isset($session_context["metadata"]["last_request"]) && $session_context["metadata"]["last_request"]) ? $session_context["metadata"]["last_request"] : "";
        $session_context["metadata"]["tmp_items"] = (isset($session_context["metadata"]["tmp_items"]) && $session_context["metadata"]["tmp_items"]) ? $session_context["metadata"]["tmp_items"] : "";

        $session_context["user"] = (isset($session_context["user"]) && $session_context["user"]) ? $session_context["user"] : [];
        $session_context["user"]["name"] = (isset($session_context["user"]["name"]) && $session_context["user"]["name"]) ? $session_context["user"]["name"] : [];
        $session_context["user"]["gender"] = (isset($session_context["user"]["gender"]) && $session_context["user"]["gender"]) ? $session_context["user"]["gender"] : [];
        $session_context["user"]["email"] = (isset($session_context["user"]["email"]) && $session_context["user"]["email"]) ? $session_context["user"]["email"] : [];
        $session_context["user"]["phone"] = (isset($session_context["user"]["phone"]) && $session_context["user"]["phone"]) ? $session_context["user"]["phone"] : [];
        $session_context["user"]["address"] = (isset($session_context["user"]["address"]) && $session_context["user"]["address"]) ? $session_context["user"]["address"] : [];

        $session_context["intents"] = (isset($session_context["intents"]) && $session_context["intents"]) ? $session_context["intents"] : [];
        $session_context["intents"]["order"] = (isset($session_context["intents"]["order"]) && $session_context["intents"]["order"]) ? $session_context["intents"]["order"] : [];
        $session_context["intents"]["order"]["address"] = (isset($session_context["intents"]["order"]["address"]) && $session_context["intents"]["order"]["address"]) ? $session_context["intents"]["order"]["address"] : [];
        $session_context["intents"]["order"]["payment_method"] = (isset($session_context["intents"]["order"]["payment_method"]) && $session_context["intents"]["order"]["payment_method"]) ? $session_context["intents"]["order"]["payment_method"] : [];

        $this->context = $session_context;

        $context = [
            "metadata" => [
                "session_id" => $session_context["metadata"]["session_id"],
                "lang" => $session_context["metadata"]["lang"],
                "date_time" => $session_context["metadata"]["date_time"],
                "timezone" => $session_context["metadata"]["timezone"],
                "last_intent" => $session_context["metadata"]["last_intent"],
                "last_request" => $session_context["metadata"]["last_request"],
                "tmp_items" => $session_context["metadata"]["tmp_items"],


            ],
            "user" => [
                "name" => $session_context["user"]["name"],
                "gender" => $session_context["user"]["gender"],
                "email" => $session_context["user"]["email"],
                "phone" => $session_context["user"]["phone"],
                "address" => $session_context["user"]["address"],
            ],
            "intents" => [
                [
                    "name" => "ordenar",
                    "position" => "3",
                    "address" => $this->getIntentAttr("ordenar", "address", false),
                    "payment_method" => $this->getIntentAttr("ordenar", "payment_method", false),
                    "status" => $this->getIntentStatus("ordenar"),
                    "confirm" => false,
                    "items" => $this->getIntentAttr("ordenar", "items", [])
                ],
                [
                    "name" => "consulta",
                    "position" => "2",
                    "status" => $this->getIntentStatus("consulta"),
                    "horarios" => [
                        "status" => $this->getIntentStatus("horarios"),
                    ],
                    "items" => [
                        "status" => $this->getIntentStatus("items"),
                    ],
                    "ingredientes" => [
                        "status" => $this->getIntentStatus("ingredientes"),
                    ],
                    "zona_envio" => [
                        "status" => $this->getIntentStatus("zona_envio"),
                    ],
                ],
                [
                    "name" => "saludar",
                    "position" => "1",
                    "status" => $this->getIntentStatus("saludar"),
                ],
                [
                    "name" => "consultar_carrito",
                    "position" => "1",
                    "status" => $this->getIntentStatus("consultar_carrito"),
                ],
                [
                    "name" => "cancelar",
                    "position" => "1",
                    "status" => $this->getIntentStatus("cancelar"),
                ],
                [
                    "name" => "despedida",
                    "status" => $this->getIntentStatus("despedida"),
                ]
            ]
        ];

        $this->parent->set("context." . $this->session_id, json_encode($context));
        $this->context = $context;
        return $this->context;
    }


    public function entitySlotsBase()
    {
        $slots = [
            "MILANESAS_GOURMET_XL" => [
                "required" => true,
                "GUARNICION_MILANESA" => "",
                "CARNE_MILANESA" => "",
                "name" => "",
                "id" => "",
                "quantity" => 1,
            ],
            "BURGERS_GOURMET" => [
                "required" => true,
                "GUARNICION_HAMBURGUESA" => "",
                "TIPO_HAMBURGUESA" => "",
                "name" => "",
                "id" => "",
                "quantity" => 1,
            ],
            "BEBIDAS" => [
                "required" => true,
                "name" => "",
                "id" => "",
                "quantity" => 1,
            ],
        ];

        return $slots;
    }


    public function getWatsonProductsCategory($category)
    {
        if ($category == "MILANESAS_GOURMET_XL") {
            return [
                ["name" => "Milanesa Yankeepower", "id" => ""],
            ];
        }

    }

    public function getWatsonTiposCarne($name)
    {
        $guarniciones = [
            ["code" => "clasica", "name" => "clasica"],
            ["code" => "juicy-lucy-rellena-de-queso-cheddar", "name" => "Juicy Lucy"],
        ];

        foreach ($guarniciones as $guarnicion) {
            if ($guarnicion["name"] == $name) {
                return $guarnicion;
            }
        }

        return false;
    }

    public function getWatsonGuarniciones($name)
    {

        $guarniciones = [
            ["code" => "papas-caseras", "name" => "Papas caseras"],
            ["code" => "papas-fritas", "name" => "Papas fritas"],
            ["code" => "papas-fritas", "name" => "Papas fritas común"],
            ["code" => "ensalada-mixta-de-lechuga-y-tomate", "name" => "Ensalada mixta"],
            ["code" => "aros-de-cebolla", "name" => "Aros de cebolla"],
        ];

        foreach ($guarniciones as $guarnicion) {
            if ($guarnicion["name"] == $name) {
                return $guarnicion;
            }
        }

        return false;
    }

    public function getWatsonProduct($name)
    {
        $products = [
            ["id" => "55", "name" => "Burger Sweet Love"],
            ["id" => "56", "name" => "Burger Dallas"],

            ["id" => "57", "name" => "Pepsi 1,5 L"],
            ["id" => "58", "name" => "Stella Artois 1 L"],
            ["id" => "62", "name" => "Agua Mineral Salus"],

            ["id" => "60", "name" => "Chivito Food & Love"],

            ["id" => "61", "name" => "Milanesa Yankeepower"],
            ["id" => "61", "name" => "Milanesa yankee con guarnición"],
        ];

        foreach ($products as $product) {
            if ($product["name"] == $name) {
                return $product;
            }
        }

        return false;
    }

    public function getProductGuarniciones($product)
    {
        $guarniciones = [];
        foreach ($product["variants"] as $variant) {
            foreach ($variant["options"] as $option) {
                if ($option["groupId"] == 22 || $option["groupId"] == 16) {
                    if (!in_array($option["name"], $guarniciones)) {
                        $guarniciones[] = $option["name"];
                    }
                }
            }
        }

        return $guarniciones;
    }

    public function getWatsonProducts()
    {
        return [];
    }

    public function getProductsVariant($productId)
    {
        $variants = [];
        $product = $this->parent->getProduct($productId, $this->session_id);
        foreach ($product["variants"] as $variant) {
            foreach ($variant["options"] as $option) {
                if (!in_array($option["code"], $variants)) {
                    $variants[] = $option["code"];
                }
            }
        }

        return $variants;
    }

    public function getCart()
    {
        $intentOrdenar = $this->getIntentContext("ordenar");
        $addItem = false;
        $orden = "";
        $ordenResult = null;

        foreach ($intentOrdenar["items"] as $item) {
            $product = $this->parent->getProduct($item["id"], $this->session_id);
            foreach ($product["variants"] as $variant) {
                $variants = $this->getProductsVariant($item["id"]);
                if (!$variants) {
                    $q = $item["quantity"];
                    $orden .= $variant["name"] . " x " . $q . "\n";
                    $addItem = true;
                } else {
                    if (array_key_exists("CARNE_MILANESA", $item)) {
                        if (isset($item["CARNE_MILANESA"]) && isset($item["GUARNICION_MILANESA"])) {
                            $variantCorrectCarne = false;
                            $variantCorrectGuarnicion = false;
                            foreach ($variant["options"] as $option) {
                                if ($option["groupId"] == "23") {
                                    if ($item["CARNE_MILANESA"] == $option["code"]) {
                                        $variantCorrectCarne = true;
                                    }
                                } elseif ($option["groupId"] == "22") {
                                    if ($item["GUARNICION_MILANESA"]["code"] == $option["code"]) {
                                        $variantCorrectGuarnicion = true;
                                    }
                                }
                                if ($variantCorrectGuarnicion && $variantCorrectCarne) {

                                    $q = $item["quantity"];
                                    $orden .= $variant["name"] . " x " . $q . "\n";
                                    $addItem = true;
                                    continue 2;
                                }
                            }
                        }
                    } elseif (array_key_exists("GUARNICION_HAMBURGUESA", $item)) {
                        if (isset($item["GUARNICION_HAMBURGUESA"]) && isset($item["TIPO_HAMBURGUESA"])) {
                            $variantCorrectGuarnicion = false;
                            $variantTipoHamburguesa = false;
                            foreach ($variant["options"] as $option) {
                                if ($option["groupId"] == "16" || $option["groupId"] == "19") {
                                    if ($item["GUARNICION_HAMBURGUESA"]["code"] == $option["code"]) {
                                        $variantCorrectGuarnicion = true;
                                    }
                                } elseif ($option["groupId"] == "18" || $option["groupId"] == "17") {
                                    if ($item["TIPO_HAMBURGUESA"]["code"] == $option["code"]) {
                                        $variantTipoHamburguesa = true;
                                    }
                                }

                                if ($variantCorrectGuarnicion && $variantTipoHamburguesa) {

                                    $addItem = true;
                                    $q = $item["quantity"];
                                    $orden .= $variant["name"] . " x " . $q . "\n";
                                    continue 2;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $orden;
    }

    public function setVendureToken($result){
        if(!$this->vendureToken){
            if(isset($result["header"]) && $result["header"]){
                foreach($result["header"] as $val){
                    $data = explode(":", $val);
                    if($data[0] == "vendure-auth-token"){
                        $this->vendureToken = $data[1];break;
                    }
                }
            }
        }
    }


    public function setProductsVariant()
    {
        $intentOrdenar = $this->getIntentContext("ordenar");
        $addItem = false;
        $orden = "";
        $ordenResult = null;
        $orderId = "";

        foreach ($intentOrdenar["items"] as $item) {
            $product = $this->parent->getProduct($item["id"], $this->session_id);
            foreach ($product["variants"] as $variant) {
                $variants = $this->getProductsVariant($item["id"]);
                if (!$variants) {
                    $ordenResult = $this->parent->addItemToOrder($variant["id"], $item["quantity"], $this->vendureToken);
                    $this->setVendureToken($ordenResult);

                    $q = $item["quantity"];
                    if ($q == 1) {
                        $q = "una";
                    }
                    $orden .= $q . " " . $variant["name"] . "\n";
                    $addItem = true;
                } else {
                    if (array_key_exists("CARNE_MILANESA", $item)) {
                        if (isset($item["CARNE_MILANESA"]) && isset($item["GUARNICION_MILANESA"])) {
                            $variantCorrectCarne = false;
                            $variantCorrectGuarnicion = false;
                            foreach ($variant["options"] as $option) {
                                if ($option["groupId"] == "23") {
                                    if ($item["CARNE_MILANESA"] == $option["code"]) {
                                        $variantCorrectCarne = true;
                                    }
                                } elseif ($option["groupId"] == "22") {
                                    if ($item["GUARNICION_MILANESA"]["code"] == $option["code"]) {
                                        $variantCorrectGuarnicion = true;
                                    }
                                }
                                if ($variantCorrectGuarnicion && $variantCorrectCarne) {
                                    $ordenResult = $this->parent->addItemToOrder($variant["id"], $item["quantity"], $this->vendureToken);
                                    $this->setVendureToken($ordenResult);
                                    $q = $item["quantity"];
                                    if ($q == 1) {
                                        $q = "una";
                                    }
                                    $orden .= $q . " " . $variant["name"] . "\n";
                                    $addItem = true;
                                    continue 2;
                                }
                            }
                        }
                    } elseif (array_key_exists("GUARNICION_HAMBURGUESA", $item)) {
                        if (isset($item["GUARNICION_HAMBURGUESA"]) && isset($item["TIPO_HAMBURGUESA"])) {
                            $variantCorrectGuarnicion = false;
                            $variantTipoHamburguesa = false;
                            foreach ($variant["options"] as $option) {
                                if ($option["groupId"] == "16" || $option["groupId"] == "19" ) {
                                    if ($item["GUARNICION_HAMBURGUESA"]["code"] == $option["code"]) {
                                        $variantCorrectGuarnicion = true;
                                    }
                                } elseif ($option["groupId"] == "18" || $option["groupId"] == "17") {
                                    if ($item["TIPO_HAMBURGUESA"]["code"] == $option["code"] ) {
                                        $variantTipoHamburguesa = true;
                                    }
                                }

                                if ($variantCorrectGuarnicion && $variantTipoHamburguesa) {
                                    $ordenResult = $this->parent->addItemToOrder($variant["id"], $item["quantity"], $this->vendureToken);
                                    $this->setVendureToken($ordenResult);
                                    $addItem = true;
                                    $q = $item["quantity"];
                                    if ($q == 1) {
                                        $q = "una";
                                    }
                                    $orden .= $q . " " . $variant["name"] . "\n";
                                    continue 2;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($addItem) {
            //if ($ordenResult) {
                //$orderId = $ordenResult["data"]["addItemToOrder"]["id"];
            //}
            $this->parent->setOrderShippingAddress($intentOrdenar["address"], $orderId, $this->phone, $this->vendureToken);
        }

        return [$addItem, $orden];
    }

    public function despedida()
    {
        $this->setContext([]);
        $this->prepareContext();
        return $this->results["messages"][] = ["type" => "text", "message" => "Gracias por comunicarse con Food and Love\n"];
    }

    public function confirmOrder()
    {

        $set = $this->setProductsVariant();
        if ($set[0]) {
            $this->parent->setOrderShippingMethod($this->vendureToken);
            $this->parent->setCustomerForOrder($this->phone, $this->vendureToken);
            $intentOrdenar = $this->getIntentContext("ordenar");
            $this->setLastIntent("continuar_comprando");
            $intentOrdenar["items"] = [];
            $this->updateItemsContext("ordenar", $intentOrdenar["items"]);

            $intentOrdenar["confirm"] = "complete";
            $intentOrdenar["payment_method"] = "";

            $this->updateOrdenar("ordenar", $intentOrdenar);
            $orden = $set[1];
            $orden = "";

            return $this->results["messages"][] = ["type" => "text", "message" => "Su orden ha sido confirmada\n\nDesea continuar comprando?\n"];
        }

        return $this->results["messages"][] = ["type" => "text", "message" => "Error creando orden\n"];
    }
}
