<?php

namespace App\Http\Controllers\AiFrameworks;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class Watson
{
    private $client;
    private $watsonApiUrl;
    private $watsonApiKey;

    public function __construct($watsonApiUrl, $watsonApiKey)
    {
        $this->watsonApiUrl = $watsonApiUrl;
        $this->watsonApiKey = $watsonApiKey;
        $this->client = new Client();
    }

    public function send($text, $session){
        $context = null;
        /*
         {
            "input":{
                "text":"mandame ravioles"
            },
            "context": {
                "conversation_id": "4035834e-d93e-4beb-844e-032d1f1efca9",
                "system": {
                    "initialized": true,
                    "dialog_stack": [
                        {
                            "dialog_node": "root"
                        }
                    ],
                    "dialog_turn_counter": 1,
                    "dialog_request_counter": 1,
                    "_node_output_map": {
                        "node_1_1481053903331": [
                            0
                        ]
                    },
                    "branch_exited": true,
                    "branch_exited_reason": "completed"
                }
            }
        }
         * */
        $body = [
            "input" => [
                "text" => $text
            ],
            "context" => [
                "conversation_id" => $session,//"4035834e-d93e-4beb-844e-032d1f1efca9",
            ]
            //"timezone" => "America/Montevideo"
        ];
        //$d = json_decode($session->metadata);
        //$d->conversation_id = "6375ff63-891d-4cd1-9d5c-f9fe864896a4";
        if(isset($session->metadata) && $session->metadata){
            $body["context"] = $session->session_id;
            $body["alternate_intents"] = false;
        }

        $response = $this->client->request('POST', $this->watsonApiUrl, [
            'auth' => [
                'apikey',
                $this->watsonApiKey
            ],
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'body' => json_encode($body),
            'verify'=>false,
        ]);

        return true;

        if($response->getStatusCode() == 200){
            if($response){
                $response = $response->getBody();

                $contents = $response->getContents();

                $response = json_decode($contents);
                /*$context = $response->context;

                if(!$session->session_id){
                    $session->session_id = md5($session->user_id . $session->contact_id . date("Y-m-d h:i:s"));
                }

                $session->metadata = json_encode($context);
                $response->output->text;
                $session->update();
                $response = json_decode($contents);*/
            }

            return $response;
        }

        return false;
    }
}
