<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('gen-calles', 'Helpers\GenCalles@index')->name('gen-calles');

Route::namespace("Bot")->group(function(){
    Route::get('/testql', '_1_0_1\IndexController@testql')->name('testql');

    Route::get('/bots/whatsapp/food-and-love', '_1_0_1\IndexController@index')->name('food-and-love')->middleware('cors');
    Route::post('/bots/whatsapp/food-and-love', '_1_0_1\IndexController@index')->name('food-and-love')->middleware('cors');

    Route::post('/watson-webhook', '_1_0_1\IndexController@reciveFromWatson')->name('watson-webhook')->middleware('cors');
});
Auth::routes();
// Registration Routes...
Route::get('register', 'HomeController@index')->name('register');
Route::post('register', 'HomeController@index');

Route::get('/home', 'HomeController@index')->name('home');

//Route::post('/watson-webhook', 'WatsonWebhookController@index')->name('watson-webhook')->middleware('cors');



